-- pour la doc du UUID: https://phauer.com/2016/uuids-hibernate-mysql/
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

CREATE TABLE IF NOT EXISTS `user` (
    `uid` varchar(36) NOT NULL primary key UNIQUE,
    `prenom` varchar(64) NOT NULL,
    `nom` varchar(64),
    `email` varchar(64) NOT NULL UNIQUE,
    `password` varchar(60) NOT NULL UNIQUE,
    `role` int(1) NOT NULL DEFAULT 1  --  Utilisateur = 0, Administrateur = 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `genre` (
    `id` int NOT NULL AUTO_INCREMENT,
    `nom_genre` varchar(64) NOT NULL,
    `img_link` varchar(64),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `playlist` (
    `id` int NOT NULL AUTO_INCREMENT, 
    `id_user` varchar(36) NOT NULL,
    `titre` varchar(255) NOT NULL,
    `privacy` int(1) NOT NULL DEFAULT 0, -- Privé = 0, Publique = 1
    -- Contraintes
    PRIMARY KEY (id),
    FOREIGN KEY (id_user) REFERENCES user(uid) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `artiste` (
    `id` int NOT NULL AUTO_INCREMENT,
    `nom_artiste` varchar(64) NOT NULL,
    `pays` varchar(64) NOT NULL,
    `img_link_artiste` varchar(255),
    -- Contraintes
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `album` (
    `id` int NOT NULL AUTO_INCREMENT, 
    `id_artiste` int NOT NULL,
    `titre_album` varchar(64) NOT NULL,
    `album_img_link` varchar(200) NOT NULL,
    -- Contraintes
    PRIMARY KEY (id),
    FOREIGN KEY (id_artiste) REFERENCES artiste(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `song` (
    `id` int NOT NULL AUTO_INCREMENT,
    `titre` varchar(64) NOT NULL,
    `link` varchar(64) NOT NULL,
    `id_artiste` int NOT NULL,
    `id_album` int,
    `id_genre` int,
    -- Contraintes
    PRIMARY KEY (`id`),
    FOREIGN KEY (id_artiste) REFERENCES artiste(id) ON DELETE CASCADE,
    FOREIGN KEY (id_album) REFERENCES album(id) ON DELETE SET NULL,
    FOREIGN KEY (id_genre) REFERENCES genre(id) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `relation_song_playlist` (
    `id_song` int NOT NULL, 
    `id_playlist` int NOT NULL,
    `date_ajout` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    -- Contraintes
    PRIMARY KEY (`id_song`,`id_playlist`),
    FOREIGN KEY (id_song) REFERENCES song(id) ON DELETE CASCADE,
    FOREIGN KEY (id_playlist) REFERENCES playlist(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
