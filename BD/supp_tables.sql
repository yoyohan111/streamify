-- Suppression des tables avec des relations
DROP TABLE IF EXISTS relation_song_playlist;
DROP TABLE IF EXISTS song;
DROP TABLE IF EXISTS album;
DROP TABLE IF EXISTS artiste;
DROP TABLE IF EXISTS playlist;
DROP TABLE IF EXISTS genre;
DROP TABLE IF EXISTS user;