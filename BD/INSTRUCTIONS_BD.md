# Instructions d'installation pour la base de donnée

**Vous pouvez utiliser la base de donnée que vous voulez sois qu'elle supporte la JDBC**

1. Créer un nouveau Shémas appelé `streamify`
2. Importer les tables dans le bon ordre (pour garder les relations), exécuter le fichier `creer_tables.sql`
3. Insérer des données avec le fichier `insert_tables.sql`
4. pour Supprimer les tables dans le bon ordre, exécuter le fichier `supp_tables.sql`