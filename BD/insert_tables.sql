SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

INSERT INTO `user` (`uid`, `prenom`, `nom`, `email`, `password`, `role`) VALUES
('840e7f55-5164-466e-983a-df6701f63cf0', 'Dominik', 'Dupuis', 'avpdom@hotmail.com', '$2a$10$gZYL2Lar1e1lxvB9U3of1.CNzr8LJdFr3lzeX7y7PYAo.G.a.mibu', 0),
('8ee308cf-f63b-4cc5-9dc1-d8637b9eb2de', 'Admin1', 'YOooo', 'admin@admin.com', '$2a$10$mbgIuJc0LoNYh8qBPIiYZOge5SWzaJi24LsBnIsHtiIZM2o5APk0W', '1'),
('7b980848-710f-42a1-9aff-bcc3f32f7be6', '123test', '', '123@123.com', '$2a$10$oNWrkvKEVvyBocyztmaT7eOXHGh3FILfxRe0l7zFBIHs.Qq7HFLOq', '0');

INSERT INTO `artiste` (`id`, `nom_artiste`, `pays`, `img_link_artiste`) VALUES
(1, 'Klaas', 'Allemagne', 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fpartyflock.nl%2Fimages%2Fartist%2F117947_1365x1365_515760%2FKlaas.jpg&f=1&nofb=1'),
(2, 'Tobu', 'Latvia', 'https://scontent.fyhu2-1.fna.fbcdn.net/v/t1.0-9/121665102_207228834095586_1265837346909408108_o.jpg?_nc_cat=110&ccb=2&_nc_sid=09cbfe&_nc_ohc=CFwLz3PMEzMAX_2Lnak&_nc_ht=scontent.fyhu2-1.fna&oh=51c31152afc9204394e8cf77f0a431f4&oe=5FDC5F36'),
(3, 'Rich Chigga', 'Indonésie', 'https://scontent.fyhu2-1.fna.fbcdn.net/v/t1.0-9/106923954_2598993017021182_1236824328358980922_n.jpg?_nc_cat=105&ccb=2&_nc_sid=09cbfe&_nc_ohc=ictI9qu9FKoAX-z4uhq&_nc_ht=scontent.fyhu2-1.fna&oh=1e6f80bccd1c2da8dc8ae3eb5290c0ee&oe=5FDEE708'),
(4, 'Martin Garrix & Tiesto', 'Néerlandais', 'https://scontent.fyhu2-1.fna.fbcdn.net/v/t1.0-9/71011198_2617586818284156_8351446040284495872_o.jpg?_nc_cat=1&ccb=2&_nc_sid=09cbfe&_nc_ohc=M2VMryRhS9cAX-Y_ZyI&_nc_ht=scontent.fyhu2-1.fna&oh=9109887563a28547663f9499e62aaeaf&oe=5FDCAA83'),
(5, 'Michael Bublé', 'Canadien', 'https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fwww.maartenschild.com%2Fsongcatcher%2Fwp-content%2Fuploads%2F2013%2F04%2Fmichael-buble.jpg&f=1&nofb=1');

INSERT INTO `album` (`id`, `id_artiste`, `titre_album`, `album_img_link`) VALUES
(1, 5, 'Christmas', 'https://upload.wikimedia.org/wikipedia/en/8/8e/MichaelBuble-Christmas%282011%29-Cover.png'),
(2, 1, 'Our own way', 'https://images.genius.com/8e590e5f976f88748528aaa0a44606b1.600x600x1.jpg'),
(3, 2, 'Colors', 'https://yt3.ggpht.com/ytc/AAUvwnhXzNKTvxM260mNWhnmwpZKZhl5M71qlzKaUdvpoQ=s900-c-k-c0x00ffffff-no-rj'),
(4, 3, 'Who that be', 'https://exclaim.ca/images/WhoThatBeArt.jpg'),
(5, 4, 'Tomorrowland - The Secret Kingdom of Melodia', 'https://images-na.ssl-images-amazon.com/images/I/A1N0Nzfx16L._AC_SX355_.jpg');

INSERT INTO `genre` (`id`, `nom_genre`, `img_link`) VALUES
(1, 'Electro', NULL),
(2, 'Hip-Hop', NULL),
(3, 'Jazz', NULL);

INSERT INTO `song` (`id`, `titre`, `link`, `id_artiste`, `id_album`, `id_genre`) VALUES
(1, 'Color', '0BxmXtsBfTtZQVFBsV1ByMEliNnM', 2, 3, 1),
(2, 'Who that be', '0BxmXtsBfTtZQaXdvX1JiUUtkZ2M', 3, 4, 2),
(3, 'The only way is up', '0BxmXtsBfTtZQYkd1aHNvRnJROFE', 4, 5, 1),
(4, 'Our own way', '1ouddslWLG_h9QDjzBXN5lRJUQ3_Ox-1h', 1, 2, 1),
(5, 'Santa Claus Is Coming To Town', '1WD7ETeNPNGNCcEtFPKFbgRkslUSlvxLS', 5, 1, 3),
(6, 'All I Want For Christmas Is You', '1WD7ETeNPNGNCcEtFPKFbgRkslUSlvxLS', 5, 1, 3);