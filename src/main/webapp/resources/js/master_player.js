//pour le navbar déroulant
$("#menu-toggle").click(function (e) {
	e.preventDefault();
	$("#wrapper").toggleClass("toggled");
});

var change_event = new Event("change");

mute_icon = document.getElementById("mute");
var audioobject = document.getElementById("audio");

function changevolume(amount) {
	audioobject.volume = amount;
	if (amount === 0) {
		mute_icon.removeClass("fa-volume-up");
		mute_icon.addClass("fa-volume-mute");
	} else {
		audioobject.muted = false;
	}
}

function mute() {
	if (mute_icon.classList.contains("fa-volume-up")) {
		$(mute_icon).addClass("fa-volume-mute");
		$(mute_icon).removeClass("fa-volume-up");
		document.getElementById("bar_volume").value = 0;
	} else if (mute_icon.classList.contains("fa-volume-mute")) {
		$(mute_icon).addClass("fa-volume-up");
		$(mute_icon).removeClass("fa-volume-mute");
		document.getElementById("bar_volume").value = 1;
	}
	document.getElementById("bar_volume").dispatchEvent(change_event); //pour que sa mute
}

function add_song_to_new_playlist(id_song, titre_song, uid_user) {
	if (id_song != null && titre_song != null && uid_user != null) {
		var nom_playlist = prompt("Ajouter la chanson à une nouvelle playlist.", "Nom de la nouvelle playlist");
	} else {
		return false;
	}
	if (classement != null) {
		window.location.href='new_playlist_with_song?id_song=' +id_song+ '&nom_playlist=' +nom_playlist+ '';	//le dernier +'' pour convertir la dernière variable en string...
		return true;
	}
}