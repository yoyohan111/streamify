console.log("Entre dans le admin_action.js");
function change_role(uid_user) {
    console.log(uid_user);
    if(uid_user != null) {
        if(confirm("Voulez-vous vraiment changer le rôle de cet Utilisateur?")){
            path = '/Streamify/change_role_user?user_uid='+ uid_user+ '';
            window.location.href=path;
            return true;
        }
    }
}
function supp_user(uid_user) {
    console.log(uid_user);
    if(uid_user != null) {
        if(confirm("Voulez-vous vraiment supprimer cet Utilisateur?")){
            path = '/Streamify/delete_user?user_uid='+ uid_user+ '';
            window.location.href=path;
            return true;
        }
    }
}

$('.mydatatable').DataTable({
    lengthMenu: [[10,25,100,-1], [10,25,100,"All"]],
    scrollY: 400,
    scrollX: true,
    scrollCollapse: true,
    "language": {
        "lengthMenu": "Afficher _MENU_ données par page",
        "zeroRecords": "Aucun compte trouvé",
        "info": "Page _PAGE_ sur _PAGES_",
        "infoEmpty": "Aucunes données disponibles",
        "infoFiltered": "(filtré à partir de _MAX_ enregistrements totaux)",
        "search":         "Recherche:",
        "thousands":      " ",
        "processing":     "En traitement...",
        "decimal":        ",",
        "paginate": {
            "first":      "Premier",
            "last":       "Dernier",
            "next":       "Prochain",
            "previous":   "Précédent"
        }
    }
});
//pour les paramêtres de language: https://datatables.net/reference/option/language