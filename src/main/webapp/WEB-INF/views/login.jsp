<%@page contentType="text/html" pageEncoding="UTF-8"%> <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="titre" scope="request" value="Connexion" />
<jsp:include page="templates/home_head.jsp" />

<section class="container-fluid mt-4 mb-4">
	<div class="row justify-content-center">
		<div class="col-11 col-sm-7 col-md-6 col-lg-4">
			<!-- messages pour l'utilisateur s'il y a lieux: -->
			<c:if test="${not empty success_msg}">
				<div class="alert alert-success" role="alert">
					<c:out value = "${success_msg}" escapeXml="false"></c:out> <!-- ici le escapeXml="false" pour que ça lis le html aulieux de juste afficher son string-->
					</div>
			</c:if>
			<c:if test="${not empty info_msg}">
				<div class="alert alert-info" role="alert">
					<c:out value = "${info_msg}" escapeXml="false"></c:out>
					</div>
			</c:if>
			<c:if test="${not empty warning_msg}">
				<div class="alert alert-warning" role="alert">
					<c:out value = "${warning_msg}" escapeXml="false"></c:out>
					</div>
			</c:if>
			<c:if test="${not empty danger_msg}">
				<div class="alert alert-danger" role="alert">
					<c:out value = "${danger_msg}" escapeXml="false"></c:out>
					</div>
			</c:if>
			
			<div class="form-group">
				<form class="form-container form_connexion" action="loginUser" method="POST">
					<div
						class="fb-login-button"
						data-size="large"
						data-button-type="continue_with"
						data-layout="rounded"
						data-auto-logout-link="false"
						data-use-continue-as="false"
						data-width=""
					></div>
					<div class="form-group">
						<h2 class="text-center font-weight-bold">Connexion</h2>
						<label for="email">Courriel: </label>
						<input type="email" class="form-control" id="email" name="email" aria-describeby="emailHelp" placeholder="Entrez votre Email" required value="${user_email}"/>
					</div>
					<div class="form-group">
						<label for="password">Mot de passe: </label>
						<input type="password" class="form-control" id="password" name="password" aria-describeby="passwordHelp" placeholder="Entrez votre mot de passe" required/>
					</div>
					<input type="submit"  class="btn btn-primary btn-block" 
					value="Se connecter"  id="submit"/>
				</form>
			</div>
		</div>
	</div>
</section>

<jsp:include page="templates/home_footer.jsp" />
