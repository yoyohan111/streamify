<%@page contentType="text/html" pageEncoding="UTF-8"%> <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="templates/player_head.jsp" />

<div class="container-fluid pt-4 pb-4">
    <!-- messages pour l'utilisateur s'il y a lieux: -->
    <c:if test="${not empty success_msg}">
        <div class="alert alert-success" role="alert">
            <c:out value = "${success_msg}" escapeXml="false"></c:out> <!-- ici le escapeXml="false" pour que ça lis le html aulieux de juste afficher son string-->
            </div>
    </c:if>
    <c:if test="${not empty warning_msg}">
        <div class="alert alert-warning" role="alert">
            <c:out value = "${warning_msg}" escapeXml="false"></c:out>
            </div>
    </c:if>
    <c:if test="${not empty danger_msg}">
        <div class="alert alert-danger" role="alert">
            <c:out value = "${danger_msg}" escapeXml="false"></c:out>
            </div>
    </c:if>

            
    <div class="d-flex justify-content-start flex-wrap">
        <a href="#" class="d-flex flex-column nodrag content_box" draggable="false">
            <div class="content_box_photo flex-grow-1">
                <img src="<c:url value="/resources/images/test_album1.jpg" />" alt="album test"> 
                <!--
                    OU
                <svg class="content_box_photo_placeholder" height="32" role="img" width="32" viewBox="-20 -25 100 100" class="_0de6546a8c9a0ed2cc34a83aa2c4a47a-scss beabeff74fb6ea16fdd40b8a78d9aeda-scss" aria-hidden="true" data-testid="card-image-fallback"><path d="M16 7.494v28.362A8.986 8.986 0 0 0 9 32.5c-4.962 0-9 4.038-9 9s4.038 9 9 9 9-4.038 9-9V9.113l30-6.378v27.031a8.983 8.983 0 0 0-7-3.356c-4.962 0-9 4.038-9 9 0 4.963 4.038 9 9 9s9-4.037 9-9V.266L16 7.494zM9 48.5c-3.86 0-7-3.14-7-7s3.14-7 7-7 7 3.14 7 7c0 3.859-3.141 7-7 7zm32-6.09c-3.86 0-7-3.14-7-7 0-3.859 3.14-7 7-7s7 3.141 7 7c0 3.861-3.141 7-7 7z" fill="currentColor" fill-rule="evenodd"></path></svg>
                -->
            </div>
            <div class="content_box_text">
                <div class="content_box_text_titre">
                    Titre
                </div>
                <div class="content_box_text_description">
                    dsfgsdfg
                </div>
            </div>
        </a>


        <a href="#" class="d-flex flex-column nodrag content_box" draggable="false">
            <div class="content_box_photo flex-grow-1">
                <svg class="content_box_photo_placeholder" height="32" role="img" width="32" viewBox="-20 -25 100 100" class="_0de6546a8c9a0ed2cc34a83aa2c4a47a-scss beabeff74fb6ea16fdd40b8a78d9aeda-scss" aria-hidden="true" data-testid="card-image-fallback"><path d="M16 7.494v28.362A8.986 8.986 0 0 0 9 32.5c-4.962 0-9 4.038-9 9s4.038 9 9 9 9-4.038 9-9V9.113l30-6.378v27.031a8.983 8.983 0 0 0-7-3.356c-4.962 0-9 4.038-9 9 0 4.963 4.038 9 9 9s9-4.037 9-9V.266L16 7.494zM9 48.5c-3.86 0-7-3.14-7-7s3.14-7 7-7 7 3.14 7 7c0 3.859-3.141 7-7 7zm32-6.09c-3.86 0-7-3.14-7-7 0-3.859 3.14-7 7-7s7 3.141 7 7c0 3.861-3.141 7-7 7z" fill="currentColor" fill-rule="evenodd"></path></svg>
            </div>
            <div class="content_box_text">
                <div class="content_box_text_titre">
                    Titre
                </div>
                <div class="content_box_text_description">
                    dsfgsdfg
                </div>
            </div>
        </a>
        <a href="#" class="d-flex flex-column nodrag content_box" draggable="false">
            <div class="content_box_photo flex-grow-1">
                <svg class="content_box_photo_placeholder" height="32" role="img" width="32" viewBox="-20 -25 100 100" class="_0de6546a8c9a0ed2cc34a83aa2c4a47a-scss beabeff74fb6ea16fdd40b8a78d9aeda-scss" aria-hidden="true" data-testid="card-image-fallback"><path d="M16 7.494v28.362A8.986 8.986 0 0 0 9 32.5c-4.962 0-9 4.038-9 9s4.038 9 9 9 9-4.038 9-9V9.113l30-6.378v27.031a8.983 8.983 0 0 0-7-3.356c-4.962 0-9 4.038-9 9 0 4.963 4.038 9 9 9s9-4.037 9-9V.266L16 7.494zM9 48.5c-3.86 0-7-3.14-7-7s3.14-7 7-7 7 3.14 7 7c0 3.859-3.141 7-7 7zm32-6.09c-3.86 0-7-3.14-7-7 0-3.859 3.14-7 7-7s7 3.141 7 7c0 3.861-3.141 7-7 7z" fill="currentColor" fill-rule="evenodd"></path></svg>
            </div>
            <div class="content_box_text">
                <div class="content_box_text_titre">
                    Titre
                </div>
                <div class="content_box_text_description">
                    dsfgsdfg
                </div>
            </div>
        </a>
        <a href="#" class="d-flex flex-column nodrag content_box" draggable="false">
            <div class="content_box_photo flex-grow-1">
                <svg class="content_box_photo_placeholder" height="32" role="img" width="32" viewBox="-20 -25 100 100" class="_0de6546a8c9a0ed2cc34a83aa2c4a47a-scss beabeff74fb6ea16fdd40b8a78d9aeda-scss" aria-hidden="true" data-testid="card-image-fallback"><path d="M16 7.494v28.362A8.986 8.986 0 0 0 9 32.5c-4.962 0-9 4.038-9 9s4.038 9 9 9 9-4.038 9-9V9.113l30-6.378v27.031a8.983 8.983 0 0 0-7-3.356c-4.962 0-9 4.038-9 9 0 4.963 4.038 9 9 9s9-4.037 9-9V.266L16 7.494zM9 48.5c-3.86 0-7-3.14-7-7s3.14-7 7-7 7 3.14 7 7c0 3.859-3.141 7-7 7zm32-6.09c-3.86 0-7-3.14-7-7 0-3.859 3.14-7 7-7s7 3.141 7 7c0 3.861-3.141 7-7 7z" fill="currentColor" fill-rule="evenodd"></path></svg>
            </div>
            <div class="content_box_text">
                <div class="content_box_text_titre">
                    Titre
                </div>
                <div class="content_box_text_description">
                    dsfgsdfg
                </div>
            </div>
        </a>
        <a href="#" class="d-flex flex-column nodrag content_box" draggable="false">
            <div class="content_box_photo flex-grow-1">
                <svg class="content_box_photo_placeholder" height="32" role="img" width="32" viewBox="-20 -25 100 100" class="_0de6546a8c9a0ed2cc34a83aa2c4a47a-scss beabeff74fb6ea16fdd40b8a78d9aeda-scss" aria-hidden="true" data-testid="card-image-fallback"><path d="M16 7.494v28.362A8.986 8.986 0 0 0 9 32.5c-4.962 0-9 4.038-9 9s4.038 9 9 9 9-4.038 9-9V9.113l30-6.378v27.031a8.983 8.983 0 0 0-7-3.356c-4.962 0-9 4.038-9 9 0 4.963 4.038 9 9 9s9-4.037 9-9V.266L16 7.494zM9 48.5c-3.86 0-7-3.14-7-7s3.14-7 7-7 7 3.14 7 7c0 3.859-3.141 7-7 7zm32-6.09c-3.86 0-7-3.14-7-7 0-3.859 3.14-7 7-7s7 3.141 7 7c0 3.861-3.141 7-7 7z" fill="currentColor" fill-rule="evenodd"></path></svg>
            </div>
            <div class="content_box_text">
                <div class="content_box_text_titre">
                    Titre
                </div>
                <div class="content_box_text_description">
                    dsfgsdfg
                </div>
            </div>
        </a>
        <a href="#" class="d-flex flex-column nodrag content_box" draggable="false">
            <div class="content_box_photo flex-grow-1">
                <svg class="content_box_photo_placeholder" height="32" role="img" width="32" viewBox="-20 -25 100 100" class="_0de6546a8c9a0ed2cc34a83aa2c4a47a-scss beabeff74fb6ea16fdd40b8a78d9aeda-scss" aria-hidden="true" data-testid="card-image-fallback"><path d="M16 7.494v28.362A8.986 8.986 0 0 0 9 32.5c-4.962 0-9 4.038-9 9s4.038 9 9 9 9-4.038 9-9V9.113l30-6.378v27.031a8.983 8.983 0 0 0-7-3.356c-4.962 0-9 4.038-9 9 0 4.963 4.038 9 9 9s9-4.037 9-9V.266L16 7.494zM9 48.5c-3.86 0-7-3.14-7-7s3.14-7 7-7 7 3.14 7 7c0 3.859-3.141 7-7 7zm32-6.09c-3.86 0-7-3.14-7-7 0-3.859 3.14-7 7-7s7 3.141 7 7c0 3.861-3.141 7-7 7z" fill="currentColor" fill-rule="evenodd"></path></svg>
            </div>
            <div class="content_box_text">
                <div class="content_box_text_titre">
                    Titre
                </div>
                <div class="content_box_text_description">
                    dsfgsdfg
                </div>
            </div>
        </a>
        <a href="#" class="d-flex flex-column nodrag content_box" draggable="false">
            <div class="content_box_photo flex-grow-1">
                <svg class="content_box_photo_placeholder" height="32" role="img" width="32" viewBox="-20 -25 100 100" class="_0de6546a8c9a0ed2cc34a83aa2c4a47a-scss beabeff74fb6ea16fdd40b8a78d9aeda-scss" aria-hidden="true" data-testid="card-image-fallback"><path d="M16 7.494v28.362A8.986 8.986 0 0 0 9 32.5c-4.962 0-9 4.038-9 9s4.038 9 9 9 9-4.038 9-9V9.113l30-6.378v27.031a8.983 8.983 0 0 0-7-3.356c-4.962 0-9 4.038-9 9 0 4.963 4.038 9 9 9s9-4.037 9-9V.266L16 7.494zM9 48.5c-3.86 0-7-3.14-7-7s3.14-7 7-7 7 3.14 7 7c0 3.859-3.141 7-7 7zm32-6.09c-3.86 0-7-3.14-7-7 0-3.859 3.14-7 7-7s7 3.141 7 7c0 3.861-3.141 7-7 7z" fill="currentColor" fill-rule="evenodd"></path></svg>
            </div>
            <div class="content_box_text">
                <div class="content_box_text_titre">
                    Titre
                </div>
                <div class="content_box_text_description">
                    dsfgsdfg
                </div>
            </div>
        </a>
        <a href="#" class="d-flex flex-column nodrag content_box" draggable="false">
            <div class="content_box_photo flex-grow-1">
                <svg class="content_box_photo_placeholder" height="32" role="img" width="32" viewBox="-20 -25 100 100" class="_0de6546a8c9a0ed2cc34a83aa2c4a47a-scss beabeff74fb6ea16fdd40b8a78d9aeda-scss" aria-hidden="true" data-testid="card-image-fallback"><path d="M16 7.494v28.362A8.986 8.986 0 0 0 9 32.5c-4.962 0-9 4.038-9 9s4.038 9 9 9 9-4.038 9-9V9.113l30-6.378v27.031a8.983 8.983 0 0 0-7-3.356c-4.962 0-9 4.038-9 9 0 4.963 4.038 9 9 9s9-4.037 9-9V.266L16 7.494zM9 48.5c-3.86 0-7-3.14-7-7s3.14-7 7-7 7 3.14 7 7c0 3.859-3.141 7-7 7zm32-6.09c-3.86 0-7-3.14-7-7 0-3.859 3.14-7 7-7s7 3.141 7 7c0 3.861-3.141 7-7 7z" fill="currentColor" fill-rule="evenodd"></path></svg>
            </div>
            <div class="content_box_text">
                <div class="content_box_text_titre">
                    Titre
                </div>
                <div class="content_box_text_description">
                    dsfgsdfg
                </div>
            </div>
        </a>
        <a href="#" class="d-flex flex-column nodrag content_box" draggable="false">
            <div class="content_box_photo flex-grow-1">
                <svg class="content_box_photo_placeholder" height="32" role="img" width="32" viewBox="-20 -25 100 100" class="_0de6546a8c9a0ed2cc34a83aa2c4a47a-scss beabeff74fb6ea16fdd40b8a78d9aeda-scss" aria-hidden="true" data-testid="card-image-fallback"><path d="M16 7.494v28.362A8.986 8.986 0 0 0 9 32.5c-4.962 0-9 4.038-9 9s4.038 9 9 9 9-4.038 9-9V9.113l30-6.378v27.031a8.983 8.983 0 0 0-7-3.356c-4.962 0-9 4.038-9 9 0 4.963 4.038 9 9 9s9-4.037 9-9V.266L16 7.494zM9 48.5c-3.86 0-7-3.14-7-7s3.14-7 7-7 7 3.14 7 7c0 3.859-3.141 7-7 7zm32-6.09c-3.86 0-7-3.14-7-7 0-3.859 3.14-7 7-7s7 3.141 7 7c0 3.861-3.141 7-7 7z" fill="currentColor" fill-rule="evenodd"></path></svg>
            </div>
            <div class="content_box_text">
                <div class="content_box_text_titre">
                    Titre
                </div>
                <div class="content_box_text_description">
                    dsfgsdfg
                </div>
            </div>
        </a>
        <a href="#" class="d-flex flex-column nodrag content_box" draggable="false">
            <div class="content_box_photo flex-grow-1">
                <svg class="content_box_photo_placeholder" height="32" role="img" width="32" viewBox="-20 -25 100 100" class="_0de6546a8c9a0ed2cc34a83aa2c4a47a-scss beabeff74fb6ea16fdd40b8a78d9aeda-scss" aria-hidden="true" data-testid="card-image-fallback"><path d="M16 7.494v28.362A8.986 8.986 0 0 0 9 32.5c-4.962 0-9 4.038-9 9s4.038 9 9 9 9-4.038 9-9V9.113l30-6.378v27.031a8.983 8.983 0 0 0-7-3.356c-4.962 0-9 4.038-9 9 0 4.963 4.038 9 9 9s9-4.037 9-9V.266L16 7.494zM9 48.5c-3.86 0-7-3.14-7-7s3.14-7 7-7 7 3.14 7 7c0 3.859-3.141 7-7 7zm32-6.09c-3.86 0-7-3.14-7-7 0-3.859 3.14-7 7-7s7 3.141 7 7c0 3.861-3.141 7-7 7z" fill="currentColor" fill-rule="evenodd"></path></svg>
            </div>
            <div class="content_box_text">
                <div class="content_box_text_titre">
                    Titre
                </div>
                <div class="content_box_text_description">
                    dsfgsdfg
                </div>
            </div>
        </a>
        <a href="#" class="d-flex flex-column nodrag content_box" draggable="false">
            <div class="content_box_photo flex-grow-1">
                <svg class="content_box_photo_placeholder" height="32" role="img" width="32" viewBox="-20 -25 100 100" class="_0de6546a8c9a0ed2cc34a83aa2c4a47a-scss beabeff74fb6ea16fdd40b8a78d9aeda-scss" aria-hidden="true" data-testid="card-image-fallback"><path d="M16 7.494v28.362A8.986 8.986 0 0 0 9 32.5c-4.962 0-9 4.038-9 9s4.038 9 9 9 9-4.038 9-9V9.113l30-6.378v27.031a8.983 8.983 0 0 0-7-3.356c-4.962 0-9 4.038-9 9 0 4.963 4.038 9 9 9s9-4.037 9-9V.266L16 7.494zM9 48.5c-3.86 0-7-3.14-7-7s3.14-7 7-7 7 3.14 7 7c0 3.859-3.141 7-7 7zm32-6.09c-3.86 0-7-3.14-7-7 0-3.859 3.14-7 7-7s7 3.141 7 7c0 3.861-3.141 7-7 7z" fill="currentColor" fill-rule="evenodd"></path></svg>
            </div>
            <div class="content_box_text">
                <div class="content_box_text_titre">
                    Titre
                </div>
                <div class="content_box_text_description">
                    dsfgsdfg
                </div>
            </div>
        </a>
        <a href="#" class="d-flex flex-column nodrag content_box" draggable="false">
            <div class="content_box_photo flex-grow-1">
                <svg class="content_box_photo_placeholder" height="32" role="img" width="32" viewBox="-20 -25 100 100" class="_0de6546a8c9a0ed2cc34a83aa2c4a47a-scss beabeff74fb6ea16fdd40b8a78d9aeda-scss" aria-hidden="true" data-testid="card-image-fallback"><path d="M16 7.494v28.362A8.986 8.986 0 0 0 9 32.5c-4.962 0-9 4.038-9 9s4.038 9 9 9 9-4.038 9-9V9.113l30-6.378v27.031a8.983 8.983 0 0 0-7-3.356c-4.962 0-9 4.038-9 9 0 4.963 4.038 9 9 9s9-4.037 9-9V.266L16 7.494zM9 48.5c-3.86 0-7-3.14-7-7s3.14-7 7-7 7 3.14 7 7c0 3.859-3.141 7-7 7zm32-6.09c-3.86 0-7-3.14-7-7 0-3.859 3.14-7 7-7s7 3.141 7 7c0 3.861-3.141 7-7 7z" fill="currentColor" fill-rule="evenodd"></path></svg>
            </div>
            <div class="content_box_text">
                <div class="content_box_text_titre">
                    Titre
                </div>
                <div class="content_box_text_description">
                    dsfgsdfg
                </div>
            </div>
        </a>
        <a href="#" class="d-flex flex-column nodrag content_box" draggable="false">
            <div class="content_box_photo flex-grow-1">
                <svg class="content_box_photo_placeholder" height="32" role="img" width="32" viewBox="-20 -25 100 100" class="_0de6546a8c9a0ed2cc34a83aa2c4a47a-scss beabeff74fb6ea16fdd40b8a78d9aeda-scss" aria-hidden="true" data-testid="card-image-fallback"><path d="M16 7.494v28.362A8.986 8.986 0 0 0 9 32.5c-4.962 0-9 4.038-9 9s4.038 9 9 9 9-4.038 9-9V9.113l30-6.378v27.031a8.983 8.983 0 0 0-7-3.356c-4.962 0-9 4.038-9 9 0 4.963 4.038 9 9 9s9-4.037 9-9V.266L16 7.494zM9 48.5c-3.86 0-7-3.14-7-7s3.14-7 7-7 7 3.14 7 7c0 3.859-3.141 7-7 7zm32-6.09c-3.86 0-7-3.14-7-7 0-3.859 3.14-7 7-7s7 3.141 7 7c0 3.861-3.141 7-7 7z" fill="currentColor" fill-rule="evenodd"></path></svg>
            </div>
            <div class="content_box_text">
                <div class="content_box_text_titre">
                    Titre
                </div>
                <div class="content_box_text_description">
                    dsfgsdfg
                </div>
            </div>
        </a>
    </div>

</div> <!-- fermer le .container-fluid -->

<jsp:include page="templates/player_footer.jsp" />