<%@page contentType="text/html" pageEncoding="UTF-8"%> <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="scss" scope="request" value="inscription" />
<c:set var="script_src" scope="request" value="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v8.0" />
<c:set var="script_src2" scope="request" value="https://code.jquery.com/jquery-3.3.1.slim.min.js" />
<c:set var="script_src3" scope="request" value="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js" /> <!-- Pour que le ValidationFormulaire.js fonctione -->
<jsp:include page="templates/home_head.jsp" />

<section class="container-fluid mt-4 mb-4">
	<div class="row justify-content-center">
		<div class="col-11 col-sm-7 col-md-6 col-lg-4">
			<div class="form-group">
				<h3 class="text-center font-weight-bold mb-3">Inscription par Facebook</h3>
				<div
					class="fb-login-button d-flex justify-content-center"
					data-size="large"
					data-button-type="continue_with"
					data-layout="rounded"
					data-auto-logout-link="false"
					data-use-continue-as="false"
					data-width=""
				></div>
			</div>
			<div id="fb-root"></div>
			<script>
				window.fbAsyncInit = function () {
					FB.init({
						appId: "355326359086813",
						autoLogAppEvents: true,
						xfbml: true,
						version: "v8.0",
					});
				};
				(function (d, s, id) {
					var js,
						fjs = d.getElementsByTagName(s)[0];
					if (d.getElementById(id)) {
						return;
					}
					js = d.createElement(s);
					js.id = id;
					js.src = "https://connect.facebook.net/fr_CA/sdk.js";
					fjs.parentNode.insertBefore(js, fjs);
				})(document, "script", "facebook-jssdk");
			</script>
			<!-- messages pour l'utilisateur s'il y a lieux: -->
			<c:if test="${not empty success_msg}">
				<div class="alert alert-success" role="alert">
					<c:out value = "${success_msg}" escapeXml="false"></c:out> <!-- ici le escapeXml="false" pour que ça lis le html aulieux de juste afficher son string-->
					</div>
			</c:if>
			<c:if test="${not empty warning_msg}">
				<div class="alert alert-warning" role="alert">
					<c:out value = "${warning_msg}" escapeXml="false"></c:out>
					</div>
			</c:if>
			<c:if test="${not empty danger_msg}">
				<div class="alert alert-danger" role="alert">
					<c:out value = "${danger_msg}" escapeXml="false"></c:out>
					</div>
			</c:if>

			<!-- TODO: faire le check de l'inscription avec javascript comme le prof avais montré
			Pour la validation avec Bootstrap 5: https://getbootstrap.com/docs/4.5/components/forms/#server-side
			-->

			<form id="signupForm" class="form_inscription form-container mt-3" action="inscrireUser" method="POST">
				<h2 class="text-center font-weight-bold">Formulaire d'inscription</h2>
				<div class="form-group">
					<label for="prenom">Prénom</label>
					<input type="text" class="form-control" id="prenom" name="prenom" placeholder="Entrez votre prénom" />
				</div>
				<div class="form-group">
					<label for="InputUsername">Nom</label>
					<input type="text" class="form-control" id="nom" name="nom" placeholder="Entrez votre nom" />
				</div>
				<div class="form-group">
					<label for="email">Courriel: </label>
					<input type="email" class="form-control" id="email" name="email" aria-describeby="emailHelp" placeholder="Entrez votre courriel" />
				</div>
				<div class="form-group">
					<label for="password">Mot de passe: </label>
					<input type="password" class="form-control" id="password" name="password" placeholder="Entrez un mot de passe" />
				</div>
				<div class="form-group">
					<label for="confirm_password">Confirmer votre mot de passe: </label>
					<input
						type="password"
						class="form-control"
						id="confirm_password"
						name="confirm_password"
						placeholder="Entrez une deuxième foix votre mot de passe"
					/>
				</div>
				<input type="submit"  class="inscription-btn btn btn-primary btn-block" 
					value="S'inscrire" id="submit" disabled="disabled"/>
			</form>
		</div>
	</div>
</section>

<script src="<c:url value="/resources/js/ValidationFormulaire.js" />"></script>
<jsp:include page="templates/home_footer.jsp" />
