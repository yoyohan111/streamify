<%@page contentType="text/html" pageEncoding="UTF-8"%> <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="templates/player_head.jsp" />

<div class="container-fluid pt-4 pb-4">

    <div class="d-flex flex-column pl-3 pr-3">
        <form method="GET" action="recherche">
            <div class="input-group">
                <form action="rechercher" method="get">
                <input type="text" name="recherche_text" class="form-control" placeholder="Recherche d'une chanson, Album ou playlist" aria-label="Recherche d'une chanson, Album ou playlist" aria-describedby="basic-addon2">
                </form>
                <div class="input-group-append">
                    
                        <button type="submit" class="btn btn-outline-secondary" type="button">Rechercher</button>

                  
                </div>
            </div>
        </form>
        <div class="card text-white bg-dark mt-3">
            <div class="card-header"><h5 class="card-title m-0">Recherche par genre :</h5></div>
            <div class="card-body">

                <strong><a href="recherche?categorie=hiphop">Hip-Hop</a> ● <a href="recherche?categorie=rock">Rock</a> ● <a href="recherche?categorie=electro">Electronic</a> ● <a href="recherche?categorie=jazz">Jazz</a></strong>
            </div>
          </div>
        <div>

        <hr>
        <h2 class="pt-3"><strong>Résultats :</strong></h2>
        <h4 class="mt-3">Chansons</h4>
        <table class="table table-dark table-hover pt-3 pl-3 mr-5">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Titre</th>
                <th scope="col">Album</th>
                <th scope="col">Date ajouté</th>
                <th class="col-1" scope="col">🕒</th>
              </tr>
            </thead>
            <tbody>
              <c:forEach var = "song" items="${listSongsByGenre}">
                <tr>
                    <th scope="row">${song.ID} <a class="recherche-row" href="" ><img class="play" data-album="${song.album.img_link}" data-titre="${song.titre}" data-artiste="${song.artiste.nom}" data-value= "${song.link}" src="<c:url value="/resources/images/play.png" />" alt="play"   ></img></a></th>
                    <td>${song.getTitre()}</td>
                    <td>${song.album.titre}</td>
                    <td>Il y a 21 jours</td>
                    <td>3:08</td>
                </tr>
              </c:forEach> 
            </tbody>
        </table>
        <hr>
        <h4 class="mt-3">Albums :</h4>
        <div class="d-flex justify-content-start flex-wrap">

            <c:forEach var = "album" items="${listAlbumsByGenre}">
                <a href="#" class="d-flex flex-column nodrag content_box" draggable="false">
                <div class="content_box_photo flex-grow-1">
                    <img id="liste_album_image" src="${album.img_link}" alt="album test"> 
                </div>
                <div class="content_box_text">
                    <div class="content_box_text_titre">
                        ${album.titre}
                    </div>
                    <div class="content_box_text_description">
                        ${album.artiste.nom}
                    </div>
                </div>
            </a>
            </c:forEach> 
        </div>
        <hr>
        <h4 class="mt-3">Playlists :</h4>
        <div class="d-flex justify-content-start flex-wrap">
            <c:forEach var = "playlist" items="${listAlbumsByGenre}">
                <a href="#" class="d-flex flex-column nodrag content_box" draggable="false">
                    <div class="content_box_photo flex-grow-1">
                        <img src="<c:url value="/resources/images/test_album1.jpg" />" alt="album test"> 
                        <!--
                            OU
                        <svg class="content_box_photo_placeholder" height="32" role="img" width="32" viewBox="-20 -25 100 100" class="_0de6546a8c9a0ed2cc34a83aa2c4a47a-scss beabeff74fb6ea16fdd40b8a78d9aeda-scss" aria-hidden="true" data-testid="card-image-fallback"><path d="M16 7.494v28.362A8.986 8.986 0 0 0 9 32.5c-4.962 0-9 4.038-9 9s4.038 9 9 9 9-4.038 9-9V9.113l30-6.378v27.031a8.983 8.983 0 0 0-7-3.356c-4.962 0-9 4.038-9 9 0 4.963 4.038 9 9 9s9-4.037 9-9V.266L16 7.494zM9 48.5c-3.86 0-7-3.14-7-7s3.14-7 7-7 7 3.14 7 7c0 3.859-3.141 7-7 7zm32-6.09c-3.86 0-7-3.14-7-7 0-3.859 3.14-7 7-7s7 3.141 7 7c0 3.861-3.141 7-7 7z" fill="currentColor" fill-rule="evenodd"></path></svg>
                        -->
                    </div>
                    <div class="content_box_text">
                        <div class="content_box_text_titre">
                            Titre
                        </div>
                        <div class="content_box_text_description">
                            dsfgsdfg
                        </div>
                    </div>
                </a>
            </c:forEach>
        </div>
        <hr>
    </div>

</div>  
<jsp:include page="templates/player_footer.jsp" />
<script>
    var JSONSongList = ${jsonList};
    var audio = document.getElementById('audio');
    var source = document.getElementById('audioSource');       
    var recherche = document.getElementsByClassName("recherche-row");
    for (var i=0; i<recherche.length; i++){
        recherche[i].onclick  = function(e) {
            e.preventDefault();
            var elm = e.target;
            document.getElementById('bar_titre_artiste').innerHTML = elm.getAttribute('data-artiste');
            document.getElementById('bar_titre_chanson').innerHTML = elm.getAttribute('data-titre');
            document.getElementById('bar_album_image').src = elm.getAttribute('data-album');
            source.src = "http://docs.google.com/uc?export=open&id=" + elm.getAttribute('data-value');
            audio.load();
            audio.play();
        };             
    }        
</script>