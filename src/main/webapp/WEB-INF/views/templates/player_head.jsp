<%@page contentType="text/html" pageEncoding="UTF-8"%> <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${empty sessionScope.user}">
    <c:set var="danger_msg" value="Vous devez vous connecter avant d'avoir accès à notre service" scope="request" />
    BUG:
    <% request.getRequestDispatcher("/src/main/webapp/WEB-INF/views/login.jsp").forward(request, response); %>
</c:if>
<!DOCTYPE html>
<html>
	<head>
		<!-- pour le titre de la page affiche sur l'onglet -->
		<c:choose>
			<c:when test="${not empty titre}">
				<title>Streamify - ${titre}</title>
			</c:when>
			<c:otherwise>
				<title>Streamify</title>
			</c:otherwise>
		</c:choose>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<!-- Bootstrap 5: https://v5.getbootstrap.com/docs/5.0/getting-started/introduction/ !-->
		<link
			href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha3/dist/css/bootstrap.min.css"
			rel="stylesheet"
			integrity="sha384-CuOF+2SnTUfTwSZjCXf01h7uYhfOBuxIhGKPbfEJ3+FqH/s6cIFN9bGr1HmAg4fQ"
			crossorigin="anonymous"
        />
        <!-- animation des icones de: https://l-lin.github.io/font-awesome-animation/ -->
        <link rel="stylesheet" href="<c:url value="/resources/css/font-awesome-animation.min.css" />"/>
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js"></script>
        <!-- scss pour toutes les pages!-->
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/master_player.scss" />"/>
        <!-- scss spécifique d'une page -->
		<c:if test="${not empty scss}">
			<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/${requestScope.scss}.scss" />"/>
        </c:if>
		<!-- jusquà 3 scripts personnel -->
		<c:if test="${not empty script_src}">
			<script src=${requestScope.script_src}></script>
		</c:if>
		<c:if test="${not empty script_src2}">
			<script src="${requestScope.script_src2}"></script>
		</c:if>
		<c:if test="${not empty script_src3}">
			<script src="${requestScope.script_src3}"></script>
		</c:if>
	</head>
	<body>

        <div class="d-flex" id="wrapper">

            <!-- Sidebar -->
            <div id="sidebar-wrapper" class="d-flex flex-column">
                <div class="d-flex flex-nowrap justify-content-center sidebar-heading border-bottom border-dark pl-3">
                    <div class="align-self-center pr-3" id="logo_container"><img class='col img-fluid' src="<c:url value="/resources/images/nav_logo.png" />" alt="Logo" \></div>
                    <span class="align-self-center"><strong>S</strong>treamify</span>
                </div>
                <div class="list-group list-group-flush mt-2">
                    <div class="list-group-item list-group-item-action main-link sidebar_item">
                        <a <c:if test = "${titre == 'Découvrir'}">class="current"</c:if> href="decouvrir">
                            <svg viewBox="0 0 512 512" width="24" height="24" xmlns="http://www.w3.org/2000/svg"><path d="M448 463.746h-149.333v-149.333h-85.334v149.333h-149.333v-315.428l192-111.746 192 110.984v316.19z" fill="currentColor"></path></svg>
                            Découvrir
                        </a>
                    </div>
                    <div class="list-group-item list-group-item-action main-link sidebar_item">
                        <a <c:if test = "${titre == 'Recherche'}">class="current"</c:if> href="recherche">
                            <svg viewBox="0 0 512 512" width="24" height="24" xmlns="http://www.w3.org/2000/svg"><path d="M357.079 341.334l94.476 110.73-32.508 27.683-94.222-110.476q-45.714 30.476-100.826 30.476-36.826 0-70.476-14.349t-57.905-38.603-38.603-57.905-14.349-70.476 14.349-70.476 38.603-57.905 57.905-38.603 70.476-14.349 70.476 14.349 57.905 38.603 38.603 57.905 14.349 70.476q0 23.365-5.841 45.714t-16.635 41.651-25.778 35.555zM224 357.079q28.19 0 53.841-11.048t44.19-29.587 29.587-44.19 11.048-53.841-11.048-53.841-29.587-44.191-44.19-29.587-53.841-11.047-53.841 11.047-44.191 29.588-29.587 44.19-11.047 53.841 11.047 53.841 29.588 44.19 44.19 29.587 53.841 11.048z" fill="currentColor"></path></svg>
                            Rechercher
                        </a>
                    </div>
                    <div class="list-group-item list-group-item-action main-link sidebar_item">
                        <a <c:if test = "${titre == 'Librairie'}">class="current"</c:if> href="librairie">
                            <svg viewBox="0 0 512 512" width="24" height="24" xmlns="http://www.w3.org/2000/svg"><path d="M311.873 77.46l166.349 373.587-39.111 17.27-166.349-373.587zM64 463.746v-384h42.666v384h-42.666zM170.667 463.746v-384h42.667v384h-42.666z" fill="currentColor"></path></svg>
                            Ma librairie
                        </a>
                    </div>

                    <hr class="hr_sidebar">
                    <div class="list-group-item list-group-item-action main-link sidebar_item">
                        <a <c:if test = "${titre == 'Favoris'}">class="current"</c:if> href="favoris">
                            <svg id="fav" role="img" height="24" width="24" viewBox="0 0 16 16" class="Svg-ulyrgf-0 hJgLcF"><path fill="none"></path><path d="M13.797 2.727a4.057 4.057 0 00-5.488-.253.558.558 0 01-.31.112.531.531 0 01-.311-.112 4.054 4.054 0 00-5.487.253c-.77.77-1.194 1.794-1.194 2.883s.424 2.113 1.168 2.855l4.462 5.223a1.791 1.791 0 002.726 0l4.435-5.195a4.052 4.052 0 001.195-2.883 4.057 4.057 0 00-1.196-2.883z"></path></svg>
                            Favoris
                        </a>
                    </div>
                    

                <hr class="hr_sidebar">
                </div> <!-- ferme du .list-group -->  
                <span class='text-uppercase font-weight-bold noselect pl-3 mb-2'>playlists</span>
                <div class="sidebar_playlists">
                    <a href="playlist?id_playlist=1" class="list-group-item list-group-item-action sidebar_item">
                        Playlist test avec id=1
                    </a>
                <c:forEach var = "playlist" items="${listePlaylist}">
                    <a href="playlist?id_playlist=${playlist.id}" class="list-group-item list-group-item-action sidebar_item">
                        ${playlist.titre}
                    </a>
                </c:forEach>

                </div>  <!-- ferme du .sidebar_playlists -->
            </div>
            <!-- /#sidebar-wrapper -->
        
            <!-- Page Content -->
            <div id='page-content-wrapper' class="d-flex flex-column">
                <nav class="navbar navbar-expand-sm navbar-dark d-flex justify-content-between pb-0">
                    <button class="pl-3 pr-3" id="menu-toggle"><span class="faa-parent animated-hover"><i
                                class="fas fa-bars fa-2x faa-horizontal"></i></span></button>
        
                    <a class="navbar-brand justify-content-center m-0">
                        <img width="30" src="<c:url value="/resources/images/nav_logo.png" />">  
                    </a>
        
        
                    <div class='d-flex align-content-end' id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item active dropdown show">
                                <a class="nav-link dropdown-toggle p-2" href="#" id="navbarDropdown" role="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="pr-2 pl-1 faa-parent animated-hover">
                                        <i class="fas fa-user fa-lg faa-float"></i>
                                    </span>
                                    <span>
                                        ${sessionScope.user.getPrenom()} ${sessionScope.user.getNom()}
                                    </span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="user">Gestion Utilisateur</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="logout">Déconnexion</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
                <main>
