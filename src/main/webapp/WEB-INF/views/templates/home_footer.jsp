<%@page contentType="text/html" pageEncoding="UTF-8"%>
        <footer class='footer'>
            <nav class='link-section'>
                <div class="top-link">
                    <dl class='footer-block'>
                        <dt class="footer-category"> Compagnie </dt>
                        <dd>
                            <a href="apropos" class="footer-link"> À Propos </a>
                        </dd>
                        <dd>
                            <a href="contact" class="footer-link"> Contact </a>
                        </dd>
                        <dd>
                            <a href="emplois" class="footer-link"> Emplois </a>
                        </dd>

                    </dl>
                    <dl class='footer-block'>
                        <dt class="footer-category"> Liens utiles </dt>
                        <dd>
                            <a href="aide" class="footer-link"> Aide </a>
                        </dd>
                        <dd>
                            <a href="player" class="footer-link"> Lecteur </a>
                        </dd>
                    </dl>
                </div>
                <div class="bottom-link">
                    <ul class="list-inline">
                        <li class='list-inline-item'>
                            <a href="check" class='nav-link' > Check la connexion </a>
                        </li>
                        <li class='list-inline-item'>
                            <a href="inscription" class='nav-link' > Inscription </a>
                        </li>
                        <li class='list-inline-item'>
                            <a href="connexion" class='nav-link' > Connexion </a>
                        </li>
                    </ul>
                    <span class="footer-legal"> © 2020 Streamify  </span>
                </div>
            </nav>
        </footer>
        <!-- Bootstrap 5 js qui insclue le popper!  -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-popRpmFF9JQgExhfw5tZT4I9/CI5e2QcuUZPOVXb1m7qUmeR2b50u+YFEYe1wgzy" crossorigin="anonymous"></script>
    </body>
</html>