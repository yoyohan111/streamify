<%@page contentType="text/html" pageEncoding="UTF-8"%> <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
</main> <!-- ferme le main -->
</div> <!-- ferme le #page-content-wrapper -->
</div> <!-- ferme le .d-flex #wrapper -->

<footer class='player-bar flex-shrink-0'>
    <div class="d-flex flex-nowrap justify-content-between align-content-center">
        <div class="now-playing-bar-item d-flex justify-content-start align-self-center" id="now-playing-bar__left">
            <div class="flex-shrink-0" id="photo_album">
                <img id="bar_album_image" src="<c:url value="/resources/images/music.svg" />" alt="album test"> 
                
            </div>
            <div class="d-flex flex-nowrap align-self-center">
                <div id="bar_titres">
                    <span id="bar_titre_chanson">${song.titre}</span><br>
                    <span id="bar_titre_artiste">${song.artiste.nom}</span>
                </div>
                <div class="flex-shrink-0 my-auto ml-3">
                    <a href="player?likeId=${song.getID()}"><svg id="bar-heart-empty" role="img" height="21" width="21" viewBox="0 0 16 16"><path d="M13.764 2.727a4.057 4.057 0 00-5.488-.253.558.558 0 01-.31.112.531.531 0 01-.311-.112 4.054 4.054 0 00-5.487.253A4.05 4.05 0 00.974 5.61c0 1.089.424 2.113 1.168 2.855l4.462 5.223a1.791 1.791 0 002.726 0l4.435-5.195A4.052 4.052 0 0014.96 5.61a4.057 4.057 0 00-1.196-2.883zm-.722 5.098L8.58 13.048c-.307.36-.921.36-1.228 0L2.864 7.797a3.072 3.072 0 01-.905-2.187c0-.826.321-1.603.905-2.187a3.091 3.091 0 012.191-.913 3.05 3.05 0 011.957.709c.041.036.408.351.954.351.531 0 .906-.31.94-.34a3.075 3.075 0 014.161.192 3.1 3.1 0 01-.025 4.403z"></path></svg></a>
                    <a href="player?unlikeId=${song.getID()}"><svg id="bar-heart-full" role="img" height="21" width="21" viewBox="0 0 16 16"><path fill="none" d="M0 0h16v16H0z"></path><path d="M13.797 2.727a4.057 4.057 0 00-5.488-.253.558.558 0 01-.31.112.531.531 0 01-.311-.112 4.054 4.054 0 00-5.487.253c-.77.77-1.194 1.794-1.194 2.883s.424 2.113 1.168 2.855l4.462 5.223a1.791 1.791 0 002.726 0l4.435-5.195a4.052 4.052 0 001.195-2.883 4.057 4.057 0 00-1.196-2.883z"></path></svg></a>
                </div>
            </div>

        </div>
        <div class="now-playing-bar-item d-flex justify-content-center align-self-center ml-3 mr-3" id="now-playing-bar__center">
            <a href="javascript:lastSong()" class="pr-4 align-self-center"><i class="fas fa-step-backward fa-2x   faa-vertical animated-hover "></i></a>
            <audio id="audio" controls controlslist="nodownload" class="flex-shrink-1" preload = "auto" autoplay loop>
                <source id="audioSource" src="http://docs.google.com/uc?export=open&id="  type="audio/mp3"/>
        
              Your browser does not support the audio element.
            </audio>
            <a href="javascript:nextSong()" class="pl-4 align-self-center"><i class="fas fa-step-forward fa-2x faa-vertical animated-hover "></i></a>
        </div>
        <div class="now-playing-bar-item d-flex justify-content-end align-self-center" id="now-playing-bar__right">
            <a href="javascript:mute()" class="p-1 align-self-center"><li id="mute" class="fas fa-lg fa-volume-up faa-wrench animated-hover"></li></a>
            <input id="bar_volume" type="range" min="0" max="1" step="0.01" onchange="changevolume(this.value)" />
        </div>
    </div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-popRpmFF9JQgExhfw5tZT4I9/CI5e2QcuUZPOVXb1m7qUmeR2b50u+YFEYe1wgzy" crossorigin="anonymous"></script>
<!-- scripts du player -->
<script src="<c:url value='/resources/js/master_player.js' />" ></script>
</body>
</html>