<%@page contentType="text/html" pageEncoding="UTF-8"%> <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<!-- pour le titre de la page affiche sur l'onglet -->
		<c:choose>
			<c:when test="${not empty titre}">
				<title>Streamify - ${titre}</title>
			</c:when>
			<c:otherwise>
				<title>Streamify</title>
			</c:otherwise>
		</c:choose>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<!-- Bootstrap 5: https://v5.getbootstrap.com/docs/5.0/getting-started/introduction/ !-->
		<link
			href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha3/dist/css/bootstrap.min.css"
			rel="stylesheet"
			integrity="sha384-CuOF+2SnTUfTwSZjCXf01h7uYhfOBuxIhGKPbfEJ3+FqH/s6cIFN9bGr1HmAg4fQ"
			crossorigin="anonymous"
		/>
		<!-- Font Awesome -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js"></script>
		<!-- scss pour toutes les pages!-->
		<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/master_home.scss" /> "/>
		<!-- scss spécifique d'une page -->
		<c:if test="${not empty scss}">
			<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/${requestScope.scss}.scss" />"/>
		</c:if>

		<!-- jusquà 3 scripts personnel -->
		<c:if test="${not empty script_src}">
			<script src="${requestScope.script_src}"></script>
		</c:if>
		<c:if test="${not empty script_src2}">
			<script src="${requestScope.script_src2}"></script>
		</c:if>
		<c:if test="${not empty script_src3}">
			<script src="${requestScope.script_src3}"></script>
		</c:if>
	</head>
<header class="banner" role="banner">
    <div class="logo-section">
        <a href="/Streamify"><img alt="Streamify" class="nav-logo" src="<c:url value="/resources/images/nav_logo.png" />"/> </a>
    </div>
    <nav class="navigation">
        <div class="navbar">
            <ul class="list-inline m-0">
                <h5>
					<c:if test="${not empty sessionScope.user}">
						<c:if test="${sessionScope.user.getRole() == 1}">
							<li class="list-inline-item">
								<a href="check" class="nav-link"> Test connexion-BD</a>
							</li>
							<li class="list-inline-item">
								<a href="admin" class="nav-link"> Contrôle Administrateur </a>
							</li>
						</c:if>
						<c:if test="${sessionScope.user.getRole() == 0}">
							<li class="list-inline-item">
								<a href="player" class="nav-link"> Lecteur de musique </a>
							</li>
						</c:if>
						<li class="list-inline-item">
							<a href="logout" class="nav-link"> Déconnexion </a>
						</li>
					</c:if>
					<c:if test="${empty sessionScope.user}">
						<li class="list-inline-item">
							<a href="inscription" class="nav-link"> Inscription </a>
						</li>
						<li class="list-inline-item">
							<a href="login" class="nav-link"> Connexion </a>
						</li>
					</c:if>
                </h5>
            </ul>
        </div>
    </nav>
</header>
<body>
