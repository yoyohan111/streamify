<%@page contentType="text/html" pageEncoding="UTF-8"%> <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="templates/player_head.jsp" />

<div id="liste_chanson_header" class="d-flex justify-content-start ">
    <div class="img_album_top align-self-end flex-shrink-0">
        <svg class="content_box_photo_placeholder" height="32" role="img" width="32" viewBox="-20 -25 100 100" class="_0de6546a8c9a0ed2cc34a83aa2c4a47a-scss beabeff74fb6ea16fdd40b8a78d9aeda-scss" aria-hidden="true" data-testid="card-image-fallback"><path d="M16 7.494v28.362A8.986 8.986 0 0 0 9 32.5c-4.962 0-9 4.038-9 9s4.038 9 9 9 9-4.038 9-9V9.113l30-6.378v27.031a8.983 8.983 0 0 0-7-3.356c-4.962 0-9 4.038-9 9 0 4.963 4.038 9 9 9s9-4.037 9-9V.266L16 7.494zM9 48.5c-3.86 0-7-3.14-7-7s3.14-7 7-7 7 3.14 7 7c0 3.859-3.141 7-7 7zm32-6.09c-3.86 0-7-3.14-7-7 0-3.859 3.14-7 7-7s7 3.141 7 7c0 3.861-3.141 7-7 7z" fill="currentColor" fill-rule="evenodd"></path></svg>
        <!-- 
            OU
        <img src="<c:url value="/resources/images/test_album1.jpg" />" alt="album test"> 
        -->
    </div>
    <div class="d-flex flex-column align-self-end">
        <p><strong>ALBUM</strong></p>
        <h1 id="titre_playlist"><strong>${album.titre}</strong></h1>
        <span id="infos_playlist">Par <a href="#"><strong>${album.artiste.nom}</strong> Pays d'origine: ${album.artiste.pays}</a> ● 14 chansons, durée de 49 min 38 sec</span>
    </div>
</div>
<div class="container-fluid pb-4">

    <div  class="d-flex flex-column">
        <div id="top_controls" class="d-flex justify-content-start align-items-center">
            <a href=""><i class="fas fa-play-circle fa-4x faa-pulse animated-hover mr-4"></i></a>
            <a href=""><i class="fas fa-pause-circle fa-4x faa-pulse animated-hover mr-4"></i></a>
            <a href=""><svg id="album-heart-empty" role="img" height="32" width="32" viewBox="0 0 32 32" class="Svg-ulyrgf-0 hJgLcF"><path d="M27.672 5.573a7.904 7.904 0 00-10.697-.489c-.004.003-.425.35-.975.35-.564 0-.965-.341-.979-.354a7.904 7.904 0 00-10.693.493A7.896 7.896 0 002 11.192c0 2.123.827 4.118 2.301 5.59l9.266 10.848a3.196 3.196 0 004.866 0l9.239-10.819A7.892 7.892 0 0030 11.192a7.896 7.896 0 00-2.328-5.619zm-.734 10.56l-9.266 10.848c-.837.979-2.508.979-3.346 0L5.035 16.104A6.9 6.9 0 013 11.192 6.9 6.9 0 015.035 6.28a6.935 6.935 0 014.913-2.048 6.89 6.89 0 014.419 1.605A2.58 2.58 0 0016 6.434c.914 0 1.555-.53 1.619-.585a6.908 6.908 0 019.346.431C28.277 7.593 29 9.337 29 11.192s-.723 3.6-2.062 4.941z"></path></svg></a> 
            <a href=""><svg id="album-heart-full" role="img" height="32" width="32" viewBox="0 0 32 32" class="Svg-ulyrgf-0 hJgLcF"><path d="M27.319 5.927a7.445 7.445 0 00-10.02-.462s-.545.469-1.299.469c-.775 0-1.299-.469-1.299-.469a7.445 7.445 0 00-10.02 10.993l9.266 10.848a2.7 2.7 0 004.106 0l9.266-10.848a7.447 7.447 0 000-10.531z"></path></svg></a>
            <div class="ml-auto align-items-center d-flex align-items-center">
                <span id="text_publique">Rendre Publique :</span>
                <label class="switch ml-3">
                    <input type="checkbox" checked>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
        <table class="table table-dark text-white table-hover mt-4">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Titre</th>
                <th scope="col">Album</th>
                <th scope="col">Date ajouté</th>
                <th class="col-1" scope="col">🕒</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">1 <a href="play"><img class="play" src="<c:url value="/resources/images/play.png" />" alt="play"></img></a></th>
                <td>Mark</td>
                <td>Otto</td>
                <td>Il y a 21 jours</td>
                <td class="text-nowrap">3:09 
                  <a class="text-nowrap ml-3" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    • • •
                  </a>
                  <div class="dropdown-menu bg-secondary border-light" aria-labelledby="dropdownMenuLink">
                    <a class="dropdown-item text-light" href="album?id=${song.getAlbum.getId}">Voir l'Album</a>
                    <a class="dropdown-item text-light" href="artiste?id=${song.getArtiste.getId}">Voir l'Artiste</a>
                    <div class="dropdown-divider bg-light"></div>
                    <a class="dropdown-item text-light" data-toggle="modal" data-target="#modal_add_playlist">Ajouter à une playlist</a>
                  </div>
                </td>
              </tr>
              <tr>
                <th scope="row">2 <a href=""><img class="pause_svg" src="<c:url value="/resources/images/pause.png" />" alt=""></a></th>
                <td>Jacob</td>
                <td>Thornton</td>
                <td>Il y a 1 jours</td>
                <td>2:49</td>
              </tr>
              <tr>
                <th scope="row">3 <a href="play"><img class="play" src="<c:url value="/resources/images/play.png" />" alt="play"></img></a></th>
                <td>Larry</td>
                <td>the Bird</td>
                <td>Il y a 3 jours</td>
                <td>1:52</td>
              </tr>
            </tbody>
          </table>
    </div>

</div> <!-- ferme le .container -->

<!-- Modal -->
<div id="modal_add_playlist" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content bg-dark text-light border-secondary">
      <div class="modal-header">
        <h5 class="modal-title">Ajouter ${song.getTitre()} à une playlist:</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="liste_playlists" class="d-flex flex-column">
          <a href="" class="d-flex align-items-center list-group-item list-group-item-action bg-dark">
            <i class="fas fa-plus-circle fa-2x mr-2"></i>
            <span>Nom d'un playlist</span>
          </a>
          <a href="" class="d-flex align-items-center list-group-item list-group-item-action bg-dark">
            <i class="fas fa-plus-circle fa-2x mr-2"></i>
            <span>Nom d'un playlist</span>
          </a>
          <a href="" class="d-flex align-items-center list-group-item list-group-item-action bg-dark">
            <i class="fas fa-plus-circle fa-2x mr-2"></i>
            <span>Nom d'un playlist</span>
          </a>
          <a href="" class="d-flex align-items-center list-group-item list-group-item-action bg-dark">
            <i class="fas fa-plus-circle fa-2x mr-2"></i>
            <span>Nom d'un playlist</span>
          </a>
          <a href="" class="d-flex align-items-center list-group-item list-group-item-action bg-dark">
            <i class="fas fa-plus-circle fa-2x mr-2"></i>
            <span>Nom d'un playlist</span>
          </a>
          <a href="" class="d-flex align-items-center list-group-item list-group-item-action bg-dark">
            <i class="fas fa-plus-circle fa-2x mr-2"></i>
            <span>Nom d'un playlist</span>
          </a>
          <a href="" class="d-flex align-items-center list-group-item list-group-item-action bg-dark">
            <i class="fas fa-plus-circle fa-2x mr-2"></i>
            <span>Nom d'un playlist</span>
          </a>
          <a href="" class="d-flex align-items-center list-group-item list-group-item-action bg-dark">
            <i class="fas fa-plus-circle fa-2x mr-2"></i>
            <span>Nom d'un playlist</span>
          </a>
          <a href="" class="d-flex align-items-center list-group-item list-group-item-action bg-dark">
            <i class="fas fa-plus-circle fa-2x mr-2"></i>
            <span>Nom d'un playlist</span>
          </a>
          <a href="" class="d-flex align-items-center list-group-item list-group-item-action bg-dark">
            <i class="fas fa-plus-circle fa-2x mr-2"></i>
            <span>Nom d'un playlist</span>
          </a>
          <a href="" class="d-flex align-items-center list-group-item list-group-item-action bg-dark">
            <i class="fas fa-plus-circle fa-2x mr-2"></i>
            <span>Nom d'un playlist</span>
          </a>
          <a href="" class="d-flex align-items-center list-group-item list-group-item-action bg-dark">
            <i class="fas fa-plus-circle fa-2x mr-2"></i>
            <span>Nom d'un playlist</span>
          </a>
          <a href="" class="d-flex align-items-center list-group-item list-group-item-action bg-dark">
            <i class="fas fa-plus-circle fa-2x mr-2"></i>
            <span>Nom d'un playlist</span>
          </a>
          <a href="" class="d-flex align-items-center list-group-item list-group-item-action bg-dark">
            <i class="fas fa-plus-circle fa-2x mr-2"></i>
            <span>Nom d'un playlist</span>
          </a>
          <a href="" class="d-flex align-items-center list-group-item list-group-item-action bg-dark">
            <i class="fas fa-plus-circle fa-2x mr-2"></i>
            <span>Nom d'un playlist</span>
          </a>
          <a href="" class="d-flex align-items-center list-group-item list-group-item-action bg-dark">
            <i class="fas fa-plus-circle fa-2x mr-2"></i>
            <span>Nom d'un playlist</span>
          </a>
          <a href="" class="d-flex align-items-center list-group-item list-group-item-action bg-dark">
            <i class="fas fa-plus-circle fa-2x mr-2"></i>
            <span>Nom d'un playlist</span>
          </a>
          <a href="" class="d-flex align-items-center list-group-item list-group-item-action bg-dark">
            <i class="fas fa-plus-circle fa-2x mr-2"></i>
            <span>Nom d'un playlist</span>
          </a>
          <a href="" class="d-flex align-items-center list-group-item list-group-item-action bg-dark">
            <i class="fas fa-plus-circle fa-2x mr-2"></i>
            <span>Nom d'un playlist</span>
          </a>
          <a href="" class="d-flex align-items-center list-group-item list-group-item-action bg-dark">
            <i class="fas fa-plus-circle fa-2x mr-2"></i>
            <span>Nom d'un playlist</span>
          </a>
          <a href="" class="d-flex align-items-center list-group-item list-group-item-action bg-dark">
            <i class="fas fa-plus-circle fa-2x mr-2"></i>
            <span>Nom d'un playlist</span>
          </a>
          <a href="" class="d-flex align-items-center list-group-item list-group-item-action bg-dark">
            <i class="fas fa-plus-circle fa-2x mr-2"></i>
            <span>Nom d'un playlist</span>
          </a>
          <a href="" class="d-flex align-items-center list-group-item list-group-item-action bg-dark">
            <i class="fas fa-plus-circle fa-2x mr-2"></i>
            <span>Nom d'un playlist</span>
          </a>
          <a href="" class="d-flex align-items-center list-group-item list-group-item-action bg-dark">
            <i class="fas fa-plus-circle fa-2x mr-2"></i>
            <span>Nom d'un playlist</span>
          </a>
          <a href="" class="d-flex align-items-center list-group-item list-group-item-action bg-dark">
            <i class="fas fa-plus-circle fa-2x mr-2"></i>
            <span>Nom d'un playlist</span>
          </a>
          <a href="" class="d-flex align-items-center list-group-item list-group-item-action bg-dark">
            <i class="fas fa-plus-circle fa-2x mr-2"></i>
            <span>Nom d'un playlist</span>
          </a>
          <a href="" class="d-flex align-items-center list-group-item list-group-item-action bg-dark">
            <i class="fas fa-plus-circle fa-2x mr-2"></i>
            <span>Nom d'un playlist</span>
          </a>


        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" onclick="add_song_to_new_playlist(${song.getId()}, ${song.getTitre()}, ${user.getUid()})"><i class="far fa-plus-square fa-lg"></i> Nouvelle playlist</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>

<jsp:include page="templates/player_footer.jsp" />