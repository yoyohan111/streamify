<%@page contentType="text/html" pageEncoding="UTF-8"%> <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="scss" scope="request" value="apropos" />
<jsp:include page="templates/home_head.jsp" />

<div class="container-fluid wrap">
	<div class="content-main">
		<div class="container">
			<h1>À Propos</h1>
			<div class="row">
				<div class="col-md-7">
					<p>Streamify est la pour vous offrire une expérience musicale unique est personnalisé.</p>
					<p>Nous mettons de l'avant une approche qui nous met en contact direct avec nos utilisateurs.</p>
					<p>Le but premier est d'offrir est un service qui est créer par vous, pour vous.</p>
					<p>Streamify est gratuit pour tous pour votre plaisir musicale.</p>
				</div>
			</div>
			<h1>L'équipe</h1>
			<div class="row">
				<h3 class="programmeur">Dominik Dupuis</h3>
				<div class="cold-md-7 programmeur">
					<img alt="Dominik" class="rounded prog-img" src="<c:url value="/resources/images/dom.jpg" />"/>
					<p class="prog-text">
						Dominik est un programmeur web qui ne recule devant rien. Il aime apprendre de nouveau framework et s'enrichire continuellement.
					</p>
				</div>
			</div>
			<div class="row">
				<h3 class="programmeur">Yohan Gagnon-Knackstedt</h3>
				<div class="cold-md-7 programmeur">
					<img alt="Yohan Gaagnon-K" class="rounded prog-img" src="<c:url value="/resources/images/yohan.jpg" />"/>
					<p class="prog-text">
						Yohan est le meilleur programmeur au monde n'avoir jamais existé sur cette planète Terre. Il aime discuter avec Dieu de ses bugs
						intermitants concernant les nouvelles technologies (Spring mvc surtout)...
					</p>
				</div>
			</div>
			<div class="row">
				<h3 class="programmeur">Nicholas Lepage</h3>
				<div class="cold-md-7 programmeur">
					<img alt="Dominik" class="rounded prog-img" src="<c:url value="/resources/images/nicholas.png" />"/>
					<p class="prog-text">Nicholas est un professeur converti en developpeur full-stack et qui est passioné de toutes choses technologie.</p>
				</div>
			</div>
		</div>
	</div>
</div>

<jsp:include page="templates/home_footer.jsp" />
