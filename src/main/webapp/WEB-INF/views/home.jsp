<%@page contentType="text/html" pageEncoding="UTF-8"%> <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="titre" scope="request" value="Home" />
<jsp:include page="templates/home_head.jsp" />

<div class="container-fluid wrap">
	<section class="welcome-block">
		<div class="welcome-headings">
			<h1 class="display-1 welcome-message primary">Tout est dans l'écoute</h1>
			<h4 class="welcome-sub-message">Votre nouvelle platforme de musique faite pour vous</h4>
			<a href="inscription" class="btn btn-default btn-home" role="button">Commencez votre écoute</a>
		</div>
	</section>
</div>

<jsp:include page="templates/home_footer.jsp" />
