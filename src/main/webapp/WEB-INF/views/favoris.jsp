<%@page contentType="text/html" pageEncoding="UTF-8"%> <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="templates/player_head.jsp" />

<div class="container-fluid pt-4 pb-4">

    <div class="d-flex flex-column pl-3 pr-3">
        <h3 class="pt-3 pb-3"><strong>Vos chansons favoris :</strong></h3>
        <table class="table table-dark table-hover pt-3 pl-3 mr-5">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Titre</th>
                <th scope="col">Album</th>
                <th scope="col">Date ajouté</th>
                <th class="col-1" scope="col">🕒</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">1 <a href="play"><img class="play" src="<c:url value="/resources/images/play.png" />" alt="play"></img></a></th>
                <td>Mark</td>
                <td>Otto</td>
                <td>Il y a 21 jours</td>
                <td>3:08</td>
              </tr>
              <tr>
                <th scope="row">2 <a href=""><img class="pause_svg" src="<c:url value="/resources/images/pause.png" />" alt=""></a></th>
                <td>Jacob</td>
                <td>Thornton</td>
                <td>Il y a 1 jours</td>
                <td>2:49</td>
              </tr>
              <tr>
                <th scope="row">3 <a href="play"><img class="play" src="<c:url value="/resources/images/play.png" />" alt="play"></img></a></th>
                <td>Larry</td>
                <td>the Bird</td>
                <td>Il y a 3 jours</td>
                <td>1:52</td>
              </tr>
            </tbody>
        </table>
        <hr>
    </div>

</div>

<jsp:include page="templates/player_footer.jsp" />