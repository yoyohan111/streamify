<%@page contentType="text/html" pageEncoding="UTF-8"%> <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="titre" scope="request" value="Admin" />
<jsp:include page="templates/home_head.jsp" />

<div class="container mt-4 mb-5">
    <h1>Gestion du compte utilisateur</h1>
    <!-- messages pour l'utilisateur s'il y a lieux: -->
    <c:if test="${not empty success_msg}">
        <div class="alert alert-success" role="alert">
            <c:out value = "${success_msg}" escapeXml="false"></c:out> <!-- ici le escapeXml="false" pour que ça lis le html aulieux de juste afficher son string-->
            </div>
    </c:if>
    <c:if test="${not empty warning_msg}">
        <div class="alert alert-warning" role="alert">
            <c:out value = "${warning_msg}" escapeXml="false"></c:out>
            </div>
    </c:if>
    <c:if test="${not empty danger_msg}">
        <div class="alert alert-danger" role="alert">
            <c:out value = "${danger_msg}" escapeXml="false"></c:out>
            </div>
    </c:if>
    <hr>
    <h3>Changer son mot de passe</h3>
    <form action="change_password" method="POST">
        <div class="form-group">
            <label for="old_password">Mot de passe actuel:</label>
            <input type="password" class="form-control" id="old_password" name="old_password" placeholder="Entrez votre mot de passe actuel" required/>
        </div>
        <div class="form-group">
            <label for="new_password">Nouveau mot de passe: </label>
            <input type="password" class="form-control" id="new_password" name="new_password" placeholder="Entrez votre nouveau mot de passe" required/>
        </div>
        <input type="submit"  class="btn btn-primary btn-block" 
        value="Changer" id="submit"/>
    </form>
</div>

<jsp:include page="templates/home_footer.jsp" />
