<%@page contentType="text/html" pageEncoding="UTF-8"%> <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="scss" scope="request" value="emplois" />
<jsp:include page="templates/home_head.jsp" />

<div class="emplois-data"> 
	<h1 class="jumbotron">Emplois</h1>
	<h3 class="emplois-h3">Merci de votre intérêt</h3>
	<p class="emplois-p">
		Malheureusement, nous ne recherchons pas d'employé pour le moment mais continuer de vérifier au cas où nous décidons de changer d'idée.
	</p>
	<h1 class="jumbotron">Critères</h1>
	<h3 class="emplois-h3">Critères recherchés</h3>
	<ul class="emplois-liste">
		<li class="emplois-li">être humain</li>
		<li class="emplois-li">Avoir des mains</li>
		<li class="emplois-li">Est capable de se d�placer</li>
		<li class="emplois-li">
			Ne pas venir d'un autre planête (nous asayons toujours d'accueilir les gens de diff�rentes cultures, mais là ça va un peux trop loin..)
		</li>
	</ul>
</div>

<jsp:include page="templates/home_footer.jsp" />
