<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<c:set var="scss" scope="request" value="check" />
<c:set var="script_src" scope="request" value="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js" />
<jsp:include page="templates/home_head.jsp" />

<%@ page isELIgnored="false" %>
	<h2>Spring MVC Database Connectivity Example</h2>
    <b>Message:</b> ${msg}
    <br/>
    <p>Artiste: <span id="artiste_nom"/> </p>
    <p>Pays: <span id="artiste_pays"/></p>
    <p>Image : </p><img id="artiste_img" src="" width="150" height="150" class="artiste-img rounded-circle" alt="alt"/>
    <p>Titre: ${song.getTitre()}</p>
    <p>Genre: ${song.genre.nom}</p>

    <p>Liste des chansons:</p>
    <ul id="list">
    <c:forEach var = "song" items="${listSongs}">
      <li><a href="" id="${song.ID}" data-value= "${song.link}"> ${song.getTitre()}</a></li>
    </c:forEach>    
    </ul>
    <button onclick="lastSong()"> < </button>
    <button onclick="nextSong()"> > </button>
    
    <audio id="audio" preload = "auto" controls>
        <source id="audioSource" src="http://docs.google.com/uc?export=open&id="  type="audio/mp3"/>

      Your browser does not support the audio element.
    </audio>
    <p> Chanson actuelle :<span id="songName"></span> <p> 
    <script>
      var audio = document.getElementById('audio');
      var source = document.getElementById('audioSource');
      var compteur = 1;
      var songList = ${jsonList};     
      console.log(songList);
      console.log(compteur);
      source.src =  "http://docs.google.com/uc?export=open&id=" + songList[compteur-1].link;
      document.getElementById('artiste_nom').innerHTML = songList[compteur-1].artiste.nom;
      document.getElementById('artiste_pays').innerHTML = songList[compteur-1].artiste.pays;
      document.getElementById('artiste_img').src = songList[compteur-1].artiste.img_link;
      document.getElementById('songName').innerHTML = songList[compteur-1].titre;   
      list.onclick = function(e) {
        e.preventDefault();
        var elm = e.target;
        source.src = "http://docs.google.com/uc?export=open&id=" + elm.getAttribute('data-value');
        compteur = elm.getAttribute('id');
        document.getElementById('artiste_nom').innerHTML = songList[compteur-1].artiste.nom;
        document.getElementById('artiste_pays').innerHTML = songList[compteur-1].artiste.pays;
        document.getElementById('artiste_img').src = songList[compteur-1].artiste.img_link;
        document.getElementById('songName').innerHTML = songList[compteur-1].titre;
        console.log(compteur);
        audio.load(); //call this to just preload the audio without playing
        audio.play();
      };
      function nextSong(){       
        compteur ++;       
        var songList = ${jsonList};                
        if(compteur > songList.length){
            compteur=1;
            source.src =  "http://docs.google.com/uc?export=open&id=" + songList[compteur-1].link;
            document.getElementById('artiste_nom').innerHTML = songList[compteur-1].artiste.nom;
            document.getElementById('artiste_pays').innerHTML = songList[compteur-1].artiste.pays;
            document.getElementById('artiste_img').src = songList[compteur-1].artiste.img_link;
            document.getElementById('songName').innerHTML = songList[compteur-1].titre;
            audio.load(); //call this to just preload the audio without playing
            audio.play();                    
        }else if (compteur === songList.length){
            source.src =  "http://docs.google.com/uc?export=open&id=" + songList[compteur-1].link;
            document.getElementById('artiste_nom').innerHTML = songList[compteur-1].artiste.nom;
            document.getElementById('artiste_pays').innerHTML = songList[compteur-1].artiste.pays;
            document.getElementById('artiste_img').src = songList[compteur-1].artiste.img_link;
            document.getElementById('songName').innerHTML = songList[compteur-1].titre;
            audio.load(); //call this to just preload the audio without playing
            audio.play(); 
        }else if(compteur < songList.length){
            document.getElementById('songName').innerHTML = songList[compteur-1].titre;
            document.getElementById('artiste_nom').innerHTML = songList[compteur-1].artiste.nom;
            document.getElementById('artiste_pays').innerHTML = songList[compteur-1].artiste.pays;
            document.getElementById('artiste_img').src = songList[compteur-1].artiste.img_link;
            source.src =  "http://docs.google.com/uc?export=open&id=" + songList[compteur-1].link;
            audio.load(); //call this to just preload the audio without playing
            audio.play();
      }};
  
      function lastSong(){
        if(audio.currentTime < 5){
            compteur --;       
            var songList = ${jsonList};
            console.log(compteur);
            if(compteur <= 0){
              compteur = songList.length;
              source.src =  "http://docs.google.com/uc?export=open&id=" + songList[songList.length-1].link;
              document.getElementById('songName').innerHTML = songList[compteur-1].titre;
              document.getElementById('artiste_nom').innerHTML = songList[compteur-1].artiste.nom;
              document.getElementById('artiste_pays').innerHTML = songList[compteur-1].artiste.pays;
              document.getElementById('artiste_img').src = songList[compteur-1].artiste.img_link;
              audio.load(); //call this to just preload the audio without playing
              audio.play();
              compteur= songList.length;
            }else if(compteur < songList.length){
              source.src =  "http://docs.google.com/uc?export=open&id=" + songList[compteur-1].link;
              document.getElementById('songName').innerHTML = songList[compteur-1].titre;
              document.getElementById('artiste_nom').innerHTML = songList[compteur-1].artiste.nom;
              document.getElementById('artiste_pays').innerHTML = songList[compteur-1].artiste.pays;
              document.getElementById('artiste_img').src = songList[compteur-1].artiste.img_link;
              audio.load(); //call this to just preload the audio without playing
              audio.play();
            }
        }else{
            audio.currentTime = 0;            
        }
};
    </script>

<jsp:include page="templates/home_footer.jsp" />