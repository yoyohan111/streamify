<%@page contentType="text/html" pageEncoding="UTF-8"%> <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="titre" scope="request" value="Admin" />
<jsp:include page="templates/home_head.jsp" />

<div class="container mt-4 mb-4">
    <h1>Panneau de contrôle de l'Admin</h1>
    <!-- messages pour l'utilisateur s'il y a lieux: -->
    <c:if test="${not empty success_msg}">
        <div class="alert alert-success" role="alert">
            <c:out value = "${success_msg}" escapeXml="false"></c:out> <!-- ici le escapeXml="false" pour que ça lis le html aulieux de juste afficher son string-->
            </div>
    </c:if>
    <c:if test="${not empty warning_msg}">
        <div class="alert alert-warning" role="alert">
            <c:out value = "${warning_msg}" escapeXml="false"></c:out>
            </div>
    </c:if>
    <c:if test="${not empty danger_msg}">
        <div class="alert alert-danger" role="alert">
            <c:out value = "${danger_msg}" escapeXml="false"></c:out>
            </div>
    </c:if>

    <hr>
    <h3>Ajouter une chanson</h3>
    <form action="ajout_chanson" method="POST">
        <div class='row'>
            <div class='col-sm-12 col-md-6 col-lg-6 form-group'>
                <label for="song_titre">Titre: </label>
                <input type="text" class="form-control" id="song_titre" name="song_titre" placeholder="Entrez le nom de la chanson" required />
            </div>
            <div class='col-sm-12 col-md-6 col-lg-6 form-group'>
                <label for="song_artiste">Artiste:</label>
                <select class="form-control" id="song_artiste" name="song_artiste" required>
                    <option value="" disabled selected>-- choisir un artiste --</option>
                    <c:forEach items="${liste_artistes}" var = "artiste">
                        <option value="${artiste.id}">${artiste.nom}</option>
                    </c:forEach>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="song_lien">Lien de la chanson: </label>
            <div class="input-group">
                <span class="input-group-text" id="basic-addon3"><i class="fas fa-link"></i></span>
                <input type="text" class="form-control" id="song_lien" name="song_lien" placeholder="Entrez le lien Google drive de la chanson" required />
            </div>
        </div>
        <div class='row'>
            <div class='col-sm-12 col-md-6 col-lg-6 form-group'>
                <div class="form-group">
                    <label for="song_album">Album:</label>
                    <select class="form-control" id="song_album" name="song_album">
                        <option value="" disabled selected>-- choisir un album --</option>
                        <c:forEach items="${liste_albums}" var = "album">
                            <option value="${album.id}">${album.titre}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class='col-sm-12 col-md-6 col-lg-6 form-group'>
                <div class="form-group">
                    <label for="song_genre">Genre:</label>
                    <select class="form-control" id="song_genre" name="song_genre">
                        <option value="" disabled selected>-- choisir un genre --</option>
                        <c:forEach items="${liste_genres}" var = "genre">
                            <option value="${genre.id}">${genre.nom}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
        </div>
        <input type="submit"  class="btn btn-primary btn-block" 
            value="Ajouter" id="song_submit"/>
    </form>
    <hr>
    <h3>Ajouter un Artiste</h3>
    <form  action="ajout_artiste" method="POST">
        <div class='row'>
            <div class='col-sm-12 col-md-6 col-lg-6 form-group'>
                <label for="artiste_nom">Nom: </label>
                <input type="text" class="form-control" id="artiste_nom" name="artiste_nom" placeholder="Entrez le nom de l'artiste" required />
            </div>
            <div class='col-sm-12 col-md-6 col-lg-6 form-group'>
                <label for="artiste_pays">Pays: </label>
                <input type="text" class="form-control" id="artiste_pays" name="artiste_pays" placeholder="Entrez le pays d'origine de l'artiste" required />
            </div>
        </div>
        <div class="form-group">
            <label for="Titre">Lien photo: </label>
            <div class="input-group">
                <span class="input-group-text" id="basic-addon3"><i class="fas fa-link"></i></span>
                <input type="text" class="form-control" id="artiste_lien" name="artiste_lien" placeholder="Entrez le lien où se trouve la photo de l'artiste" />
                <span class="input-group-text" id="basic-addon1"><i class="fas fa-file-image"></i></span>
            </div>
        </div>
        <input type="submit"  class="btn btn-primary btn-block" 
            value="Ajouter" id="artiste_submit"/>
    </form>
    <hr>
    <h3>Ajouter un Album</h3>
    <form  action="ajout_album" method="POST">
        <div class='row'>
            <div class='col-sm-12 col-md-6 col-lg-6 form-group'>
                <label for="album_titre">Titre: </label>
                <input type="text" class="form-control" id="album_titre" name="album_titre" placeholder="Entrez le titre de l'album" required/>
            </div>
            <div class='col-sm-12 col-md-6 col-lg-6 form-group'>
                <label for="album_artiste">Artiste:</label>
                <select class="form-control" id="album_artiste" name="album_artiste" required>
                    <option value="" disabled selected>-- choisir un artiste --</option>
                    <c:forEach items="${liste_artistes}" var = "artiste">
                        <option value="${artiste.id}">${artiste.nom}</option>
                    </c:forEach>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="Titre">Lien photo: </label>
            <div class="input-group">
                <span class="input-group-text" id="basic-addon3"><i class="fas fa-link"></i></span>
                <input type="text" class="form-control" id="album_lien" name="album_lien" placeholder="Entrez le lien où se trouve la photo de l'album" required/>
                <span class="input-group-text" id="basic-addon1"><i class="fas fa-file-image"></i></span>
            </div>
        </div>
        <input type="submit"  class="btn btn-primary btn-block" 
            value="Ajouter" id="album_submit"/>
    </form>
    <hr>
    <h3>Ajouter un genre</h3>
    <form  action="ajout_genre" method="POST">
        <div class='row'>
            <div class='col-sm-12 col-md-4 col-lg-4 form-group'>
                <label for="Titre">Nom: </label>
                <input type="text" class="form-control" id="genre_nom" name="genre_nom" placeholder="Entrez le nom du genre" required/>
            </div>
            <div class='col-sm-12 col-md-8 col-lg-8 form-group'>
                <label for="Titre">Lien photo: </label>
                <div class="input-group">
                    <span class="input-group-text" id="basic-addon3"><i class="fas fa-link"></i></span>
                    <input type="text" class="form-control" id="genre_lien" name="genre_lien" placeholder="Entrez le lien où se trouve la photo du genre" />
                    <span class="input-group-text" id="basic-addon1"><i class="fas fa-file-image"></i></span>
                </div>
            </div>
        </div>
        <input type="submit"  class="btn btn-primary btn-block" 
            value="Ajouter" id="genre_submit"/>
    </form>
    <hr>
    <div class="card mb-4">
        <div class="card-header">
            <h3 class='my-auto'><i class="fas fa-users-cog"></i>  Panneau d'utilisateurs:</h3>
        </div>
        <div class="card-body">
            <table class="table table-striped mydatatable" style="width: 100%">
                <thead>
                    <tr>
                        <th class="text-nowrap"><i class="fas fa-id-badge"></i> UId</th>
                        <th class="text-nowrap"><i class="fas fa-address-card"></i> Nom et Prenom</th>
                        <th class="text-nowrap"><i class="fas fa-at"></i> Email   <i class="far fa-envelope"></i></th>
                        <th class="text-nowrap"><i class="fas fa-user-shield"></i> Admin</th>
                        <th class="text-nowrap"><i class="fas fa-trash-alt"></i> Supprimer</th>
                    </tr>
                </thead>
                <tbody class="body-datatable">
                    <c:forEach items="${liste_users}" var = "user">
                        <tr class='my-auto table-active'>
                            <td class='text-nowrap my-auto'>${user.uid}</td>
                            <td class='text-nowrap my-auto'>${user.prenom} ${user.nom}</td>
                            <td class='text-nowrap my-auto'>${user.email}</td>
                            <td class='text-nowrap my-auto'>
                                <div class="form-check form-switch">
                                    <input class="form-check-input" type="checkbox" id="flexSwitchCheckChecked" onclick="change_role('${user.uid}')"
                                    <c:if test = "${user.getRole() == 1}">
                                        checked
                                     </c:if>
                                    >
                                    <label class="form-check-label" for="flexSwitchCheckChecked"></label>
                                </div>
                            </td>
                            <td class="text-center text-nowrap col-1"><a href="javascript:supp_user('${user.uid}');"><i class="fas fa-times fa-lg text-danger"></i></a></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- jQuery CDN POUR QUE LE DATATABLE FONCTIONNE! (mais brise le ValidationFormulaire.js si inclu dans cette page...) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> 

<!-- Pour le DataTable -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.20/r-2.2.3/sc-2.0.1/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.20/r-2.2.3/sc-2.0.1/datatables.min.js"></script>

<script src="<c:url value='/resources/js/admin_action.js' />" ></script>

<jsp:include page="templates/home_footer.jsp" />
