<%@page contentType="text/html" pageEncoding="UTF-8"%> <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="templates/player_head.jsp" />

<div id="liste_chanson_header" class="d-flex justify-content-start ">
    <div class="img_album_top align-self-end flex-shrink-0">
        <svg class="content_box_photo_placeholder" height="32" role="img" width="32" viewBox="-20 -25 100 100" class="_0de6546a8c9a0ed2cc34a83aa2c4a47a-scss beabeff74fb6ea16fdd40b8a78d9aeda-scss" aria-hidden="true" data-testid="card-image-fallback"><path d="M16 7.494v28.362A8.986 8.986 0 0 0 9 32.5c-4.962 0-9 4.038-9 9s4.038 9 9 9 9-4.038 9-9V9.113l30-6.378v27.031a8.983 8.983 0 0 0-7-3.356c-4.962 0-9 4.038-9 9 0 4.963 4.038 9 9 9s9-4.037 9-9V.266L16 7.494zM9 48.5c-3.86 0-7-3.14-7-7s3.14-7 7-7 7 3.14 7 7c0 3.859-3.141 7-7 7zm32-6.09c-3.86 0-7-3.14-7-7 0-3.859 3.14-7 7-7s7 3.141 7 7c0 3.861-3.141 7-7 7z" fill="currentColor" fill-rule="evenodd"></path></svg>
        <!-- 
            OU
        <img src="<c:url value="/resources/images/test_album1.jpg" />" alt="album test"> 
        -->
    </div>
    <div class="d-flex flex-column align-self-end">
        <p><strong>PLAYLIST</strong></p>
        <h1 id="titre_playlist"><strong>Titre du playlist ou album..</strong></h1>
        <span id="infos_playlist">Par <a href="#"><strong>[nom de l'artiste]</strong></a> ● 14 chansons, durée de 49 min 38 sec</span>
    </div>
</div>
<div class="container-fluid pb-4">

    <div  class="d-flex flex-column">
        <div id="top_controls" class="d-flex justify-content-start align-items-center">
            <a href=""><i class="fas fa-play-circle fa-4x faa-pulse animated-hover mr-4"></i></a>
            <a href=""><i class="fas fa-pause-circle fa-4x faa-pulse animated-hover mr-4"></i></a>
            <a href=""><svg id="album-heart-empty" role="img" height="32" width="32" viewBox="0 0 32 32" class="Svg-ulyrgf-0 hJgLcF"><path d="M27.672 5.573a7.904 7.904 0 00-10.697-.489c-.004.003-.425.35-.975.35-.564 0-.965-.341-.979-.354a7.904 7.904 0 00-10.693.493A7.896 7.896 0 002 11.192c0 2.123.827 4.118 2.301 5.59l9.266 10.848a3.196 3.196 0 004.866 0l9.239-10.819A7.892 7.892 0 0030 11.192a7.896 7.896 0 00-2.328-5.619zm-.734 10.56l-9.266 10.848c-.837.979-2.508.979-3.346 0L5.035 16.104A6.9 6.9 0 013 11.192 6.9 6.9 0 015.035 6.28a6.935 6.935 0 014.913-2.048 6.89 6.89 0 014.419 1.605A2.58 2.58 0 0016 6.434c.914 0 1.555-.53 1.619-.585a6.908 6.908 0 019.346.431C28.277 7.593 29 9.337 29 11.192s-.723 3.6-2.062 4.941z"></path></svg></a> 
            <a href=""><svg id="album-heart-full" role="img" height="32" width="32" viewBox="0 0 32 32" class="Svg-ulyrgf-0 hJgLcF"><path d="M27.319 5.927a7.445 7.445 0 00-10.02-.462s-.545.469-1.299.469c-.775 0-1.299-.469-1.299-.469a7.445 7.445 0 00-10.02 10.993l9.266 10.848a2.7 2.7 0 004.106 0l9.266-10.848a7.447 7.447 0 000-10.531z"></path></svg></a>
            <div class="ml-auto align-items-center d-flex align-items-center">
                <span id="text_publique">Rendre Publique :</span>
                <label class="switch ml-3">
                    <input type="checkbox" checked>
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
        <table class="table table-dark text-white table-hover mt-4">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Titre</th>
                <th scope="col">Album</th>
                <th scope="col">Date ajouté</th>
                <th class="col-1" scope="col">🕒</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">1 <a href="play"><img class="play" src="<c:url value="/resources/images/play.png" />" alt="play"></img></a></th>
                <td>Mark</td>
                <td>Otto</td>
                <td>Il y a 21 jours</td>
                <td class="text-nowrap">
                  <a href="liker_song?song_id=${song.getID()}"><svg id="bar-heart-empty" role="img" height="21" width="21" viewBox="0 0 16 16"><path d="M13.764 2.727a4.057 4.057 0 00-5.488-.253.558.558 0 01-.31.112.531.531 0 01-.311-.112 4.054 4.054 0 00-5.487.253A4.05 4.05 0 00.974 5.61c0 1.089.424 2.113 1.168 2.855l4.462 5.223a1.791 1.791 0 002.726 0l4.435-5.195A4.052 4.052 0 0014.96 5.61a4.057 4.057 0 00-1.196-2.883zm-.722 5.098L8.58 13.048c-.307.36-.921.36-1.228 0L2.864 7.797a3.072 3.072 0 01-.905-2.187c0-.826.321-1.603.905-2.187a3.091 3.091 0 012.191-.913 3.05 3.05 0 011.957.709c.041.036.408.351.954.351.531 0 .906-.31.94-.34a3.075 3.075 0 014.161.192 3.1 3.1 0 01-.025 4.403z"></path></svg></a>
                  <a href="liker_song?song_id=${song.getID()}"><svg id="bar-heart-full" role="img" height="21" width="21" viewBox="0 0 16 16"><path fill="none" d="M0 0h16v16H0z"></path><path d="M13.797 2.727a4.057 4.057 0 00-5.488-.253.558.558 0 01-.31.112.531.531 0 01-.311-.112 4.054 4.054 0 00-5.487.253c-.77.77-1.194 1.794-1.194 2.883s.424 2.113 1.168 2.855l4.462 5.223a1.791 1.791 0 002.726 0l4.435-5.195a4.052 4.052 0 001.195-2.883 4.057 4.057 0 00-1.196-2.883z"></path></svg></a>
                  <span> 3:09 </span>
                  <a class="text-nowrap ml-3" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    • • •
                  </a>
                  <div class="dropdown-menu bg-secondary border-light" aria-labelledby="dropdownMenuLink">
                    <a class="dropdown-item text-light" href="album?id=${song.getAlbum.getId}">Voir l'Album</a>
                    <a class="dropdown-item text-light" href="artiste?id=${song.getArtiste.getId}">Voir l'Artiste</a>
                    <div class="dropdown-divider bg-light"></div>
                    <a class="dropdown-item text-light" data-toggle="modal" data-target="#modal_add_playlist">Ajouter à une playlist</a>
                  </div>
                </td>
              </tr>
              <tr>
                <th scope="row">2 <a href=""><img class="pause_svg" src="<c:url value="/resources/images/pause.png" />" alt=""></a></th>
                <td>Jacob</td>
                <td>Thornton</td>
                <td>Il y a 1 jours</td>
                <td>2:49</td>
              </tr>
              <tr>
                <th scope="row">3 <a href="play"><img class="play" src="<c:url value="/resources/images/play.png" />" alt="play"></img></a></th>
                <td>Larry</td>
                <td>the Bird</td>
                <td>Il y a 3 jours</td>
                <td>1:52</td>
              </tr>
            </tbody>
          </table>
    </div>

</div> <!-- ferme le .container -->
<input name="id_song" value="" type="hidden" />	<!-- Pour savoir le id de la chanson dont l'utilisateur a pesé le menu -->

<!-- Modal -->
<div id="modal_add_playlist" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content bg-dark text-light border-secondary">
      <div class="modal-header">
        <h5 class="modal-title">Ajouter ${song.getTitre()} à une playlist:</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="liste_playlists" class="d-flex flex-column">
          <a href="ajout_song_to_playlist?id_playlist=&${playlist.id}id_song=" class="d-flex align-items-center list-group-item list-group-item-action bg-dark">
            <i class="fas fa-plus-circle fa-2x mr-2"></i>
            <span>Nom d'un playlist</span>
          </a>
          <a href="ajout_song_to_playlist?id_playlist=&${playlist.id}id_song=" class="d-flex align-items-center list-group-item list-group-item-action bg-dark">
            <i class="fas fa-plus-circle fa-2x mr-2"></i>
            <span>Nom d'un autre playlist...</span>
          </a>
          <a href="ajout_song_to_playlist?id_playlist=&${playlist.id}id_song=" class="d-flex align-items-center list-group-item list-group-item-action bg-dark">
            <i class="fas fa-plus-circle fa-2x mr-2"></i>
            <span>Nom d'un playlist</span>
          </a>
          <a href="ajout_song_to_playlist?id_playlist=&${playlist.id}id_song=" class="d-flex align-items-center list-group-item list-group-item-action bg-dark">
            <i class="fas fa-plus-circle fa-2x mr-2"></i>
            <span>Nom d'un playlist</span>
          </a>
          <a href="ajout_song_to_playlist?id_playlist=&${playlist.id}id_song=" class="d-flex align-items-center list-group-item list-group-item-action bg-dark">
            <i class="fas fa-plus-circle fa-2x mr-2"></i>
            <span>Nom d'un playlist</span>
          </a>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" onclick="new_playlist('${playlist.id}')"><i class="far fa-plus-square fa-lg"></i> Nouvelle playlist</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>


<script src="<c:url value='/resources/js/song_action.js' />" ></script>
<jsp:include page="templates/player_footer.jsp" />
<script>
  var JSONSongList = ${jsonList};
  var audio = document.getElementById('audio');
  var source = document.getElementById('audioSource');       
  var recherche = document.getElementsByClassName("recherche-row");
  for (var i=0; i<recherche.length; i++){
      recherche[i].onclick  = function(e) {
          e.preventDefault();
          var elm = e.target;
          document.getElementById('bar_titre_artiste').innerHTML = elm.getAttribute('data-artiste');
          document.getElementById('bar_titre_chanson').innerHTML = elm.getAttribute('data-titre');
          document.getElementById('bar_album_image').src = elm.getAttribute('data-album');
          source.src = "http://docs.google.com/uc?export=open&id=" + elm.getAttribute('data-value');
          audio.load();
          audio.play();
      };             
  }        
</script>