/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.streamify.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.streamify.model.Genre;
import com.streamify.model.Song;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author Dominik
 */
public class JSONCreator {
    
    public JSONCreator(){
        
    }
    
    public String createListSongJSON(List<Song> listSong){
        ObjectMapper mapper = new ObjectMapper();
        try{
            String jsonString = mapper.writeValueAsString(listSong);
            return jsonString;
        } catch (IOException e){
            return e.toString();
        }
    }
    
    public String createListGenreJSON(List<Genre> listGenre){
        ObjectMapper mapper = new ObjectMapper();
        try{
            String jsonString = mapper.writeValueAsString(listGenre);
            return jsonString;
        } catch (IOException e){
            return e.toString();
        }
    }
}
