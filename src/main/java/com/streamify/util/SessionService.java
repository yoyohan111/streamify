package com.streamify.util;

import javax.servlet.http.HttpSession;

import com.streamify.model.User;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class SessionService {
    private static ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
    private static HttpSession session = attr.getRequest().getSession(false); // true == allow create

    public static boolean is_connected() {
        if (session != null) {
            if (session.getAttribute("user") != null){
                return true;
            }
        }
        return false;
    }
    public static User get_connected_user() {
        if (is_connected()){
            User user = (User)session.getAttribute("user");
            return user;
        }
        return null;
    }
    public static boolean is_connected_as_user() {
        //Dois checker is_connected() en premier, ninon ça génère un null pointerException  :-)        
        return is_connected() && get_connected_user().getRole() == 0;
    }
    public static boolean is_connected_as_admin() {
        return is_connected() && get_connected_user().getRole() == 1;
    }
}
