/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.streamify.dao;

import com.streamify.model.User;
import com.streamify.util.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserDAO {
    public static User findByUid(UUID uid){
            String sqlCommand = "SELECT * from user where uid = ?";
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch(ClassNotFoundException e){
            System.out.println(e);
        }
        try{
            PreparedStatement ps = DBConnection.getConnection().prepareStatement(sqlCommand);
            ps.setObject(1, uid.toString());
            ResultSet rs = ps.executeQuery();
            User user = new User();
            while(rs.next()){
                String userPrenom = rs.getString("prenom");
                String userNom = rs.getString("nom");
                String userEmail = rs.getString("email");
                String userPassword = rs.getString("password");
                int userRole = rs.getInt("role");
                user = new User(uid,userPrenom,userNom,userEmail,userPassword,userRole);
            }
            DBConnection.closeConnection();
            return user;
        } catch(SQLException e){
            System.out.println(e);
        }
        return null;
    }

    public static User findByEmail(String email){
        String sqlCommand = "SELECT * from user where email = ?";
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch(ClassNotFoundException e){
            System.out.println(e);
        }
        try{
            PreparedStatement ps = DBConnection.getConnection().prepareStatement(sqlCommand);
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            User user = new User();
            while(rs.next()){
                UUID UserUid = UUID.fromString(rs.getString("uid"));
                String userPrenom = rs.getString("prenom");
                String userNom = rs.getString("nom");
                String userEmail = rs.getString("email");
                String userPassword = rs.getString("password");
                int userRole = rs.getInt("role");
                user = new User(UserUid,userPrenom,userNom,userEmail,userPassword,userRole);
            }
            DBConnection.closeConnection();
            if (user.getEmail() == null){
                return null;
            }
            return user;
        } catch(SQLException e){
            System.out.println(e);
        }
        return null;
    }
    public static List<User> getAll(){
        String sql = "SELECT * from user";
        try{
            PreparedStatement ps = DBConnection.getConnection().prepareStatement(sql);            
            ResultSet rs = ps.executeQuery();
            List<User> listeUser = new ArrayList<>();
            User user = null;
            while(rs.next()){
                UUID UserUid = UUID.fromString(rs.getString("uid"));
                String userPrenom = rs.getString("prenom");
                String userNom = rs.getString("nom");
                String userEmail = rs.getString("email");
                String userPassword = rs.getString("password");
                int userRole = rs.getInt("role");
                user = new User(UserUid,userPrenom,userNom,userEmail,userPassword,userRole);
                listeUser.add(user);
            }
            DBConnection.closeConnection();
            return listeUser;
        } catch(SQLException e){
            List<User> listeUserVide = new ArrayList<>();
            return listeUserVide;
        }
    }
    public static boolean insert(User user){
        String sqlCommand = "INSERT INTO user (uid, prenom, nom, email, password, role) VALUES (?,?,?,?,?,?)";
        try{
            if(findByEmail(user.getEmail()) == null){
                PreparedStatement ps = DBConnection.getConnection().prepareStatement(sqlCommand);
                ps.setString(1, user.getUid().toString());
                ps.setString(2, user.getPrenom());
                ps.setString(3, user.getNom());
                ps.setString(4, user.getEmail());
                ps.setString(5,user.getPassword());
                ps.setInt(6, user.getRole());
                ps.executeUpdate();
                DBConnection.closeConnection();
                return true;
            }
            
        } catch(SQLException e){
            System.out.println(e.toString());
        }
        return false;
    }
    
    public static boolean update(User user){
        String sqlCommand = "UPDATE user SET prenom = ?, nom = ?, email = ?, password = ?, role = ? WHERE uid = ?";
        try{
            if(findByEmail(user.getEmail()) != null){
                PreparedStatement ps = DBConnection.getConnection().prepareStatement(sqlCommand);
                ps.setString(1, user.getPrenom());
                ps.setString(2, user.getNom());
                ps.setString(3, user.getEmail());
                ps.setString(4,user.getPassword());
                ps.setInt(5, user.getRole());
                ps.setString(6, user.getUid().toString());
                ps.executeUpdate();
                DBConnection.closeConnection();
                return true;
            }
        } catch(SQLException e){
            System.out.println(e.toString());
        }
        return false;
    }
    public static boolean change_role(String user_uid){
        String sqlCommand = "UPDATE user SET role = ? WHERE uid = ?";
        try{
            User user = new User();
            user.setUidFromString(user_uid);
            if (findByUid(user.getUid()) != null) {
                PreparedStatement ps = DBConnection.getConnection().prepareStatement(sqlCommand);
                if (findByUid(user.getUid()).getRole() == 1) {
                    ps.setInt(1, 0);
                } else {
                    ps.setInt(1, 1);
                }
                ps.setString(2, user_uid);
                ps.executeUpdate();
                DBConnection.closeConnection();
                return true;
            }
        } catch(SQLException e){
            System.out.println(e.toString());
        }
        return false;
    }
    public static boolean delete(String user_uid){
        String sqlCommand = "delete from user where uid = ?";
		boolean retour = false;
		int nbLigne = 0;
		try {
			PreparedStatement ps = DBConnection.getConnection().prepareStatement(sqlCommand);
			ps.setString(1,user_uid);

            nbLigne = ps.executeUpdate();
            DBConnection.closeConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(nbLigne >0 )
			retour = true;		
		return retour;
    }
}
