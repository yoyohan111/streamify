/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.streamify.dao;

import com.streamify.model.Playlist;
import com.streamify.model.Song;

import java.util.List;
import java.util.UUID;

/**
 *
 * @author Dominik
 */
public interface PlaylistDAO {
    Playlist getById(int id);
    List<Song> getSongsByPlaylistId(int id_playlist);
    List<Playlist> getPlaylistsUser(UUID userId);
    List<Playlist> getPlaylistsByName(String nom);
    boolean insert(Playlist playlist);
    boolean insertSongInPlaylist(int id_song, int id_playlist);
}
