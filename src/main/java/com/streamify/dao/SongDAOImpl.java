/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.streamify.dao;

import com.streamify.model.Album;
import com.streamify.model.Artiste;
import com.streamify.model.Genre;
import java.util.List;
import com.streamify.model.Song;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import com.streamify.util.JSONCreator;
import com.streamify.util.DBConnection;

/**
 *
 * @author Dominik
 */
public class SongDAOImpl implements SongDAO{
    private JSONCreator jsonCreator = new JSONCreator();
    
    

    
    @Override
    public String getSongLink(int id){
        String sqlSong = "SELECT link from song where ID = ?";

        try{
            PreparedStatement ps = DBConnection.getConnection().prepareStatement(sqlSong);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            String songLink ="";
            while(rs.next()){
                songLink = rs.getString("link");
            }
            DBConnection.closeConnection();
            return songLink;
        } catch(SQLException e){
            return "";
        }
        
    }
    
    @Override
    public Song getSongById(int id){
        String sqlSong = 
                "SELECT *, artiste.*, genre.* from song\n" +
                "INNER JOIN artiste ON artiste.id=song.id_artiste \n" +
                "INNER JOIN genre ON genre.id=song.id_genre\n" +
                "INNER JOIN album ON album.id=song.id_album\n" +
                "WHERE song.id = ?";
      
        try{
            PreparedStatement ps = DBConnection.getConnection().prepareStatement(sqlSong);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            Song song = new Song();
            while(rs.next()){
                Artiste artiste = new Artiste(rs.getInt("id_artiste"), rs.getString("nom_artiste"),rs.getString("pays"),rs.getString("img_link_artiste"));
                Album songAlbum = new Album(rs.getInt("id_album"),artiste,rs.getString("titre_album"));
                Genre songGenre = new Genre(rs.getInt("id_genre"),rs.getString("nom_genre"));
                song = new Song(rs.getInt("id"),rs.getString("titre"),rs.getString("link"),songAlbum,songGenre);
            }
            DBConnection.closeConnection();
            return song;
        } catch(SQLException e){
            Song songVide = new Song();
            return songVide;
        }
        
    }
    
    @Override
    public List<Song> getAllSongs(){
        String sqlSong = "SELECT *, artiste.*, genre.* from song\n" +
                "INNER JOIN artiste ON artiste.id=song.id_artiste \n" +
                "INNER JOIN genre ON genre.id=song.id_genre\n" +
                "INNER JOIN album ON album.id=song.id_album\n";  
        
        try{
            PreparedStatement ps = DBConnection.getConnection().prepareStatement(sqlSong);            
            ResultSet rs = ps.executeQuery();
            List<Song> listeSong = new ArrayList<>();
           
            while(rs.next()){      
                Song song = new Song();
                int songId = rs.getInt("id");
                String songTitre = rs.getString("titre");
                String songLink = rs.getString("link");
                Artiste artiste = new Artiste(rs.getInt("id_artiste"), rs.getString("nom_artiste"),rs.getString("pays"),rs.getString("img_link_artiste"));
                Album songAlbum = new Album(rs.getInt("id_album"),artiste,rs.getString("titre_album"), rs.getString("album_img_link"));
                Genre songGenre = new Genre(rs.getInt("id_genre"),rs.getString("nom_genre"));
                song = new Song(songId,songTitre,songLink,songAlbum,songGenre);
                listeSong.add(song);
            }
            DBConnection.closeConnection();
            return listeSong;
        } catch(SQLException e){
            List<Song> listeSongVide = new ArrayList<>();
            return listeSongVide;
        }
        
    }
    
    @Override
    public List<Song> getSongsByGenre(int id){
        String sqlSongByGenre = "SELECT *, artiste.*, genre.* from song\n" +
                "INNER JOIN artiste ON artiste.id=song.id_artiste \n" +
                "INNER JOIN genre ON genre.id=song.id_genre\n" +
                "INNER JOIN album ON album.id=song.id_album\n" +
                "WHERE song.id_genre = ?";
        
        try{
            PreparedStatement ps = DBConnection.getConnection().prepareStatement(sqlSongByGenre);   
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            List<Song> listeSong = new ArrayList<>();
           
            while(rs.next()){      
                Song song = new Song();
                int songId = rs.getInt("id");
                String songTitre = rs.getString("titre");
                String songLink = rs.getString("link");
                Artiste artiste = new Artiste(rs.getInt("id_artiste"), rs.getString("nom_artiste"),rs.getString("pays"),rs.getString("img_link_artiste"));
                Album songAlbum = new Album(rs.getInt("id_album"),artiste,rs.getString("titre_album"), rs.getString("album_img_link"));
                Genre songGenre = new Genre(rs.getInt("id_genre"),rs.getString("nom_genre"));
                song = new Song(songId,songTitre,songLink,songAlbum,songGenre);
                listeSong.add(song);
            }
            DBConnection.closeConnection();
            return listeSong;
        } catch(SQLException e){
            List<Song> listeSongVide = new ArrayList<>();
            return listeSongVide;
        }
    }
    
    @Override
    public String getListeSongJSON(List<Song> listeSong){
        String listeSongJSON = jsonCreator.createListSongJSON(listeSong);
        return listeSongJSON;
    }
    
    @Override
    public String getSongGenre(int id){
        String songGenreSQL = "SELECT genre.nom FROM song INNER JOIN genre ON song.id_genre=genre.id WHERE id_genre = ?";
        
        try{
            PreparedStatement ps = DBConnection.getConnection().prepareStatement(songGenreSQL);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            String songGenre = "";
            while(rs.next()){                             
                songGenre = rs.getString("nom");
               
            }
            DBConnection.closeConnection();
            return songGenre;
        } catch(SQLException e){
            return "";
        }
    }
    
    
    @Override
    public List<Song> getSongsByName(String nom){
        String sqlSongByGenre = "SELECT *, artiste.*, genre.* from song\n" +
                "INNER JOIN artiste ON artiste.id=song.id_artiste \n" +
                "INNER JOIN genre ON genre.id=song.id_genre\n" +
                "INNER JOIN album ON album.id=song.id_album\n" +
                "WHERE song.id_genre = ?";

        try{
            PreparedStatement ps = DBConnection.getConnection().prepareStatement(sqlSongByGenre);   
            ps.setString(1, nom);
            ResultSet rs = ps.executeQuery();
            List<Song> listeSong = new ArrayList<>();

            while(rs.next()){      
                Song song = new Song();
                int songId = rs.getInt("id");
                String songTitre = rs.getString("titre");
                String songLink = rs.getString("link");
                Artiste artiste = new Artiste(rs.getInt("id_artiste"), rs.getString("nom_artiste"),rs.getString("pays"),rs.getString("img_link_artiste"));
                Album songAlbum = new Album(rs.getInt("id_album"),artiste,rs.getString("titre_album"), rs.getString("album_img_link"));
                Genre songGenre = new Genre(rs.getInt("id_genre"),rs.getString("nom_genre"));
                song = new Song(songId,songTitre,songLink,songAlbum,songGenre);
                listeSong.add(song);
            }
            DBConnection.closeConnection();
            return listeSong;
        } catch(SQLException e){
            List<Song> listeSongVide = new ArrayList<>();
            return listeSongVide;
        }
    }
    
    @Override
    public boolean insert(Song song){
        String sqlCommand = "INSERT INTO song (titre, link, id_artiste, id_album, id_genre) VALUES (?,?,?,?,?)";
        try{
                PreparedStatement ps = DBConnection.getConnection().prepareStatement(sqlCommand);
                ps.setString(1, song.getTitre());
                ps.setString(2, song.getLink());
                ps.setInt(3, song.getAlbum().getArtiste().getId());
                ps.setInt(4,song.getAlbum().getId());
                ps.setInt(5, song.getGenre().getId());
                ps.executeUpdate();
                DBConnection.closeConnection();
                return true;
        } catch(SQLException e){
            System.out.println(e.toString());
        }
        return false;
    }
}
