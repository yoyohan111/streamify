
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.streamify.dao;

import com.streamify.model.Artiste;
import com.streamify.util.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author bobynet
 */
public class ArtisteDAOImpl implements ArtisteDAO{
    
   @Override
   public Artiste getById(int id){
       String sqlArtisteBySong = "SELECT * FROM artiste WHERE id = ?";
       
        try{
            PreparedStatement ps = DBConnection.getConnection().prepareStatement(sqlArtisteBySong);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            Artiste artiste = new Artiste();
            while(rs.next()){
                int idArtiste = rs.getInt("id");
                String nom = rs.getString("nom");
                String pays = rs.getString("pays");
                String img_link = rs.getString("img_link");
                artiste = new Artiste(idArtiste,nom,pays,img_link);
            }
            DBConnection.closeConnection();
            return artiste;
        } catch(SQLException e){
            return new Artiste();
        }
   }
   
   @Override
   public List<Artiste> getAllArtistes(){
       String sqlArtisteBySong = "SELECT * FROM artiste";
       
        try{
            PreparedStatement ps = DBConnection.getConnection().prepareStatement(sqlArtisteBySong);
            ResultSet rs = ps.executeQuery();
            List<Artiste> listeArtiste = new ArrayList<>();
            while(rs.next()){
                int idArtiste = rs.getInt("id");
                String nom = rs.getString("nom_artiste");
                String pays = rs.getString("pays");
                String img_link = rs.getString("img_link_artiste");
                Artiste artiste = new Artiste(idArtiste,nom,pays,img_link);
                listeArtiste.add(artiste);
            }
            DBConnection.closeConnection();
            return listeArtiste;
        } catch(SQLException e){
            List<Artiste> listeArtisteVide = new ArrayList<>();
            return listeArtisteVide;
        }
   }
   
   @Override
   public boolean insert(Artiste artiste){
       String sqlCommand = "INSERT INTO artiste (nom_artiste, pays, img_link_artiste) VALUES (?,?,?)";
        try{
            if(artiste != null){
                PreparedStatement ps = DBConnection.getConnection().prepareStatement(sqlCommand);
                ps.setString(1, artiste.getNom());
                ps.setString(2, artiste.getPays());
                ps.setString(3, artiste.getImg_link());
                ps.executeUpdate();
                DBConnection.closeConnection();
                return true;
            }
        } catch(SQLException e){
            System.out.println(e.toString());
        }
        return false;
   }
}