/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.streamify.dao;

import com.streamify.model.Album;
import java.util.List;

/**
 *
 * @author Dominik
 */
public interface AlbumDAO {
   List<Album> getAlbumByGenre(int id);
   Album getAlbumById(int id);
   List<Album> getAllAlbums();
   boolean insert(Album album);
}
