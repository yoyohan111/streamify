/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.streamify.dao;

import com.streamify.model.Album;
import com.streamify.model.Artiste;
import com.streamify.model.Genre;
import com.streamify.model.Playlist;
import com.streamify.model.Song;
import com.streamify.util.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author Dominik
 */
public class PlaylistDAOImpl implements PlaylistDAO{
    public Playlist getById(int id){
        String sqlArtisteBySong = "SELECT * FROM playlist WHERE id = ?";
        try{
             Class.forName("com.mysql.cj.jdbc.Driver");
         } catch(ClassNotFoundException e){
             
         }
         
         try{
             PreparedStatement ps = DBConnection.getConnection().prepareStatement(sqlArtisteBySong);
             ps.setInt(1, id);
             ResultSet rs = ps.executeQuery();
             Playlist playlist = new Playlist();
             while(rs.next()){
                 int idPlaylist = rs.getInt("id");
                 String user_uid = rs.getString("id_user");
                 String titre = rs.getString("titre");
                 int privacy = rs.getInt("privacy");
                 
                 playlist.setId(id);
                 playlist.setUidUserFromString(user_uid);
                 playlist.setTitre(titre);
                 playlist.setPrivacy(privacy);
             }
             DBConnection.closeConnection();
             return playlist;
         } catch(SQLException e){
             return new Playlist();
         }
    }
    @Override
    public List<Song> getSongsByPlaylistId(int id_playlist){
        String sqlSongByGenre = "SELECT *, artiste.*, genre.* from song\n" +
            "INNER JOIN artiste ON artiste.id=song.id_artiste \n" +
            "INNER JOIN genre ON genre.id=song.id_genre\n" +
            "INNER JOIN album ON album.id=song.id_album\n" +
            "WHERE song.id IN (SELECT DISTINCT id_song FROM relation_song_playlist WHERE id_playlist = ? ORDER BY relation_song_playlist.date_ajout DESC)";
        try{
            PreparedStatement ps = DBConnection.getConnection().prepareStatement(sqlSongByGenre);   
            ps.setInt(1, id_playlist);
            ResultSet rs = ps.executeQuery();
            List<Song> listeSong = new ArrayList<>();
            
            while(rs.next()){      
                Song song = new Song();
                int songId = rs.getInt("id");
                String songTitre = rs.getString("titre");
                String songLink = rs.getString("link");
                Artiste artiste = new Artiste(rs.getInt("id_artiste"), rs.getString("nom_artiste"),rs.getString("pays"),rs.getString("img_link_artiste"));
                Album songAlbum = new Album(rs.getInt("id_album"),artiste,rs.getString("titre_album"), rs.getString("album_img_link"));
                Genre songGenre = new Genre(rs.getInt("id_genre"),rs.getString("nom_genre"));
                song = new Song(songId,songTitre,songLink,songAlbum,songGenre);
                listeSong.add(song);
            }
            DBConnection.closeConnection();
            return listeSong;
        } catch(SQLException e){
            List<Song> listeSongVide = new ArrayList<>();
            return listeSongVide;
        }
    }

    @Override
    public List<Playlist> getPlaylistsUser(UUID userId){
        String sqlAllGenres = "SELECT * FROM playlist WHERE id_user = ?";

        try{
            PreparedStatement ps = DBConnection.getConnection().prepareStatement(sqlAllGenres);    
            ps.setString(1, userId.toString());
            ResultSet rs = ps.executeQuery();            
            List<Playlist> listeGenre = new ArrayList<>();
            while(rs.next()){
                Playlist playlist = new Playlist(rs.getInt("id"),UUID.fromString(rs.getString("id_user")),rs.getString("titre"),rs.getInt("privacy"));
                listeGenre.add(playlist);
            }
            DBConnection.closeConnection();
            return listeGenre;
        } catch(SQLException e){
            List<Playlist> listePlaylistVide = new ArrayList<>();
            return listePlaylistVide;
        }
    }
    
    @Override
    public List<Playlist> getPlaylistsByName(String nom){
        String sqlAllGenres = "SELECT * FROM playlist WHERE titre = ?";

        try{
            PreparedStatement ps = DBConnection.getConnection().prepareStatement(sqlAllGenres);    
            ps.setString(1, nom);
            ResultSet rs = ps.executeQuery();            
            List<Playlist> listeGenre = new ArrayList<>();
            while(rs.next()){
                Playlist playlist = new Playlist(rs.getInt("id"),UUID.fromString(rs.getString("id_user")),rs.getString("titre"),rs.getInt("privacy"));
                listeGenre.add(playlist);
            }
            DBConnection.closeConnection();
            return listeGenre;
        } catch(SQLException e){
            List<Playlist> listePlaylistVide = new ArrayList<>();
            return listePlaylistVide;
        }
    }
    @Override
    public boolean insert(Playlist playlist){
        String sqlCommand = "INSERT INTO playlist (id_user, titre) VALUES (?,?)";
        try{
                PreparedStatement ps = DBConnection.getConnection().prepareStatement(sqlCommand);
                ps.setString(1, playlist.getId_user().toString());
                ps.setString(2, playlist.getTitre());
                ps.executeUpdate();
                DBConnection.closeConnection();
                return true;
        } catch(SQLException e){
            System.out.println(e.toString());
        }
        return false;
    }
    
    @Override
    public boolean insertSongInPlaylist(int id_song, int id_playlist){
        String sqlCommand = "INSERT INTO relation_song_playlist (id_song, id_playlist) VALUES (?,?)";
        try{
                PreparedStatement ps = DBConnection.getConnection().prepareStatement(sqlCommand);
                ps.setInt(1, id_song);
                ps.setInt(2, id_playlist);
                ps.executeUpdate();
                DBConnection.closeConnection();
                return true;
        } catch(SQLException e){
            System.out.println(e.toString());
        }
        return false;
    }
}
