/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.streamify.dao;

import com.streamify.model.Genre;
import com.streamify.util.DBConnection;
import com.streamify.util.JSONCreator;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Dominik
 */
public class GenreDAOImpl implements GenreDAO {

    private final JSONCreator jsonCreator = new JSONCreator();

    public GenreDAOImpl(){

    }

    @Override
    public List<Genre> getAllGenres(){
        String sqlAllGenres = "SELECT * FROM genre";

        try{
            PreparedStatement ps = DBConnection.getConnection().prepareStatement(sqlAllGenres);            
            ResultSet rs = ps.executeQuery();
            List<Genre> listeGenre = new ArrayList<>();
            while(rs.next()){
                int genreId = rs.getInt("id");
                String genreNom = rs.getString("nom_genre");
                Genre genre = new Genre(genreId,genreNom);
                listeGenre.add(genre);
            }
            DBConnection.closeConnection();
            return listeGenre;
        } catch(SQLException e){
            List<Genre> listeGenreVide = new ArrayList<>();
            return listeGenreVide;
        }
    }

    @Override
    public String getListeGenreJSON(List<Genre> listeGenre){
        String listeSongJSON = jsonCreator.createListGenreJSON(listeGenre);
        return listeSongJSON;
    }
    
    @Override
    public boolean insert(Genre genre){
       String sqlCommand = "INSERT INTO genre (nom_genre, img_link) VALUES (?,?)";
        try{
                PreparedStatement ps = DBConnection.getConnection().prepareStatement(sqlCommand);
                ps.setString(1, genre.getNom());
                ps.setString(2, genre.getImg_link());
                ps.executeUpdate();
                DBConnection.closeConnection();
                return true;            
        } catch(SQLException e){
            System.out.println(e.toString());
        }
        return false;
    }
}
