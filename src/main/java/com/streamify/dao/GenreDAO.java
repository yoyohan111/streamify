/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.streamify.dao;

import com.streamify.model.Genre;
import java.util.List;


/**
 *
 * @author Dominik
 */
public interface GenreDAO {
    public List <Genre> getAllGenres();
    String getListeGenreJSON(List<Genre> listeGenre);
    boolean insert(Genre genre);
}
