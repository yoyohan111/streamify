
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.streamify.dao;

import com.streamify.model.Album;
import com.streamify.model.Artiste;
import com.streamify.util.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 *
 * @author Dominik
 */
public class AlbumDAOImpl implements AlbumDAO {
    
    @Override
    public List<Album> getAlbumByGenre(int id){
        String sqlSongByGenre = "SELECT *, artiste.* from album\n" +
                "INNER JOIN artiste ON artiste.id=album.id_artiste \n" +
                "INNER JOIN song ON song.id_album=album.id\n" +
                "WHERE song.id_genre = ?";
        
        try{
            PreparedStatement ps = DBConnection.getConnection().prepareStatement(sqlSongByGenre);   
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            HashSet<Album> set = new HashSet<>();
           
            while(rs.next()){      
                Artiste artiste = new Artiste(rs.getInt("id_artiste"), rs.getString("nom_artiste"),rs.getString("pays"),rs.getString("img_link_artiste"));
                Album album = new Album(rs.getInt("id_album"),artiste,rs.getString("titre_album"), rs.getString("album_img_link"));
                set.add(album);
            }
            DBConnection.closeConnection();
            
            List<Album>listeAlbumFiltrer = new ArrayList<>(set);
            return listeAlbumFiltrer;
        } catch(SQLException e){
            List<Album> listeAlbumVide = new ArrayList<>();
            return listeAlbumVide;
        }
    }
    
    @Override
    public Album getAlbumById(int id){
        String sql = "SELECT ALBUM.ID, "+
                    "ALBUM.ID_ARTISTE, "+
                    "ALBUM.TITRE_ALBUM, "+
                    "ALBUM.ALBUM_IMG_LINK, "+
                    "ARTISTE.NOM_ARTISTE, "+
                    "ARTISTE.PAYS, "+
                    "ARTISTE.IMG_LINK_ARTISTE "+
                    "FROM ARTISTE "+
                    "INNER JOIN ALBUM "+
                    "ON ARTISTE.ID  = ALBUM.ID_ARTISTE "+
                    "WHERE ALBUM.ID = ?";       
        try{
            PreparedStatement ps = DBConnection.getConnection().prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            Album album = null;
            while(rs.next()){
                int idAlbum = rs.getInt("ID");
                int idArtiste = rs.getInt("ID_ARTISTE");
                String titreAlbum = rs.getString("TITRE_ALBUM");
                String photoAlbum = rs.getString("ALBUM_IMG_LINK");
                
                
                String nomArtiste = rs.getString("NOM_ARTISTE");
                String paysArtiste = rs.getString("PAYS");
                String photoArtiste = rs.getString("IMG_LINK_ARTISTE");

                Artiste artiste = null;
                if (idArtiste != 0) {
                    artiste = new Artiste(idArtiste,nomArtiste,paysArtiste,photoArtiste);
                }
                album = new Album(idAlbum,artiste,titreAlbum,photoAlbum);
            }
            DBConnection.closeConnection();
            return album;
        } catch(SQLException e){
            return new Album();
        }
    }
    
    @Override
    public List<Album> getAllAlbums(){
        String sql = "SELECT ALBUM.ID, "+
                    "ALBUM.ID_ARTISTE, "+
                    "ALBUM.TITRE_ALBUM, "+
                    "ALBUM.ALBUM_IMG_LINK, "+
                    "ARTISTE.NOM_ARTISTE, "+
                    "ARTISTE.PAYS, "+
                    "ARTISTE.IMG_LINK_ARTISTE "+
                    "FROM ARTISTE "+
                    "INNER JOIN ALBUM "+
                    "ON ARTISTE.ID  = ALBUM.ID_ARTISTE";
        try{
            PreparedStatement ps = DBConnection.getConnection().prepareStatement(sql);            
            ResultSet rs = ps.executeQuery();
            List<Album> listeAlbum = new ArrayList<>();
            Album album = null;
            while(rs.next()){
                int idAlbum = rs.getInt("ID");
                int idArtiste = rs.getInt("ID_ARTISTE");
                String titreAlbum = rs.getString("TITRE_ALBUM");
                String photoAlbum = rs.getString("ALBUM_IMG_LINK");
                
                
                String nomArtiste = rs.getString("NOM_ARTISTE");
                String paysArtiste = rs.getString("PAYS");
                String photoArtiste = rs.getString("IMG_LINK_ARTISTE");

                Artiste artiste = null;
                if (idArtiste != 0) {
                    artiste = new Artiste(idArtiste,nomArtiste,paysArtiste,photoArtiste);
                }
                album = new Album(idAlbum,artiste,titreAlbum,photoAlbum);
                listeAlbum.add(album);
            }
            DBConnection.closeConnection();
            return listeAlbum;
        } catch(SQLException e){
            List<Album> listeAlbumVide = new ArrayList<>();
            return listeAlbumVide;
        }
    }
    
    @Override
    public boolean insert(Album album){
        String sqlCommand = "INSERT INTO album(id_artiste, titre_album, album_img_link) VALUES (?,?,?)";
        try{
            if(album!=null){
               PreparedStatement ps = DBConnection.getConnection().prepareStatement(sqlCommand);
               ps.setInt(1, album.getArtiste().getId());
               ps.setString(2, album.getTitre());
               ps.setString(3, album.getImg_link());
               ps.executeUpdate();
               DBConnection.closeConnection();
               return true;
            }            
        } catch (SQLException e){
            System.out.println(e.toString());
        }
        return false;
    }
}

