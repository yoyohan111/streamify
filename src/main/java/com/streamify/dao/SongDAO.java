package com.streamify.dao;

import java.util.List;
import com.streamify.model.Song;

public interface SongDAO {
    String getSongLink(int id);
    Song getSongById(int id);
    List<Song> getAllSongs();
    String getSongGenre(int id);
    List<Song> getSongsByGenre(int id);
    List<Song> getSongsByName(String nom);
    String getListeSongJSON(List<Song> listeSong);
    boolean insert(Song song);
}
