package com.streamify.model;

public class Song {
    private int ID;
    private String titre;
    private String link;
    private Album album;    
    private Genre genre;
    private String date_ajout;

    public Song(){

    }
    
    //Constructeurs
    //sans id_album ni id_genre
    public Song (int ID, String titre, String link){
        this.ID = ID;
        this.titre = titre;
        this.link = link;
    }
    //avec tout les paramêtres

    public Song(int ID, String titre, String link, Album album, Genre genre) {
        this.ID = ID;
        this.titre = titre;
        this.link = link;
        this.album = album;
        this.genre = genre;
    }
    
    
    public int getID(){
        return ID;    
    }

    public void setID(int ID){
        this.ID = ID;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Album getAlbum() {
        return album;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }
    
    public String getDate_ajout() {
        return date_ajout;
    }

    public void setDate_ajout(String date_ajout) {
        this.date_ajout = date_ajout;
    }
    
    @Override
    public String toString() {
        return "Song{" + "ID=" + ID + ", titre=" + titre + ", link=" + link + ", album=" + album.getTitre() + ", genre=" + genre.getNom() + '}';
    }
}
