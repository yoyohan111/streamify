/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.streamify.model;

import java.util.UUID;

/**
 *
 * @author Dominik
 */
public class Playlist {
    private int id;
    private UUID id_user;
    private String titre;
    private int privacy;

    public Playlist() { }
    
    public Playlist(int id, UUID id_user, String titre, int privacy){
        this.id = id;
        this.id_user = id_user;
        this.titre = titre;
        this.privacy = privacy;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UUID getId_user() {
        return id_user;
    }

    public void setId_user(UUID id_user) {
        this.id_user = id_user;
    }
    public void setUidUserFromString(String uid) {
        UUID uuid = UUID.fromString(uid);
        this.id_user = uuid;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public int getPrivacy() {
        return privacy;
    }

    public void setPrivacy(int privacy) {
        this.privacy = privacy;
    }
}
