/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.streamify.model;

import java.util.Objects;

/**
 *
 * @author Dominik
 */
public class Album {
    private int id;
    private Artiste artiste;
    private String titre;
    private String img_link;
    
    public Album(){
        
    }
    
    public Album(int id, Artiste artiste, String titre){
        this.id = id;
        this.artiste = artiste;
        this.titre = titre;
    }
    
    public Album(int id, Artiste artiste, String titre, String img_link){
        this.id = id;
        this.artiste = artiste;
        this.titre = titre;
        this.img_link = img_link;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Artiste getArtiste() {
        return artiste;
    }

    public void setArtiste(Artiste artiste) {
        this.artiste = artiste;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getImg_link() {
        return img_link;
    }

    public void setImg_link(String img_link) {
        this.img_link = img_link;
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Album other = (Album) obj;
        if (this.id != other.id) {
            return false;
        }
        return Objects.equals(this.titre, other.titre);
    }
    
}
