/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.streamify.model;

/**
 *
 * @author bobynet
 */
public class Artiste {
    private int id;
    private String nom;
    private String pays;
    private String img_link;
    
    public Artiste(){
        
    }
    
    public Artiste(int id, String nom, String pays, String img_link) {
        this.id = id;
        this.nom = nom;
        this.pays = pays;
        this.img_link = img_link;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getImg_link() {
        return img_link;
    }

    public void setImg_link(String img_link) {
        this.img_link = img_link;
    }
    
    
    
}
