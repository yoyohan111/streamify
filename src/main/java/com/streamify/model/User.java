/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.streamify.model;

import java.util.UUID;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 *
 * @author Dominik
 */
public class User {
    BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    private UUID uid;
    private String prenom;
    private String nom;
    private String email;
    private String password;
    private int role;
    
    public User(){
        this.uid= UUID.randomUUID();
    }

    //constructeur avec tout les paramêtres
    public User(UUID uid, String prenom, String nom, String email, String password, int role) {
        this.uid = uid;
        this.prenom = prenom;
        this.nom = nom;
        this.email = email;
        this.password = password;
        this.role = role;
    }

    public UUID getUid() {
        return uid;
    }

    public void setId(UUID uid) {
        this.uid = uid;
    }
    public void setUidFromString(String uid) {
        UUID uuid = UUID.fromString(uid);
        this.uid = uuid;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }    

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }
    
    public void encodePassword() //pour la sécurité
    {
        this.password = passwordEncoder.encode(this.password);
    }
    
    public boolean isEncodedPasswordMatch (String rawPassword) {
        return passwordEncoder.matches(rawPassword, this.password);  //the rawPassword must be the first parameter, and the encodedPassword the second.
    }
    
    @Override
    public String toString() {
        return "User{" + ", uid=" + uid + ", prenom=" + prenom + ", nom=" + nom + ", email=" + email + ", password=" + password + ", role=" + role + '}';
    }
}
