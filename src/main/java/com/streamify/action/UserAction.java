package com.streamify.action;

import java.util.UUID;
import com.streamify.dao.UserDAO;
import com.streamify.model.User;

public class UserAction {
    public static User findByUid(UUID uid){
        return UserDAO.findByUid(uid);
    }
    public static User findByEmail(String email){
        return UserDAO.findByEmail(email);
    }
    public static boolean inscrireUser(User user){
        user.encodePassword();  //encoder le password
        user.setRole(0);
        return UserDAO.insert(user);
    }
    public static boolean inscrireAdmin(User admin){
        admin.encodePassword();  //encoder le password
        admin.setRole(1);
        return UserDAO.insert(admin);
    }
    public static boolean updatePassword(UUID uid, String pass) {
        User user = UserDAO.findByUid(uid);
        if (user != null) {
            user.setPassword(pass);
            System.out.println(user.toString());
            user.encodePassword();  //encoder le password
            return UserDAO.update(user);
        } else {
            return false;
        }
    }  
}
