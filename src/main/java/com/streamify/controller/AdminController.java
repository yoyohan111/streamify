package com.streamify.controller;

import com.streamify.model.Album;
import com.streamify.model.Artiste;
import com.streamify.model.Genre;
import com.streamify.model.Song;
import com.streamify.model.User;
import com.streamify.util.SessionService;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

import com.streamify.dao.AlbumDAOImpl;
import com.streamify.dao.ArtisteDAOImpl;
import com.streamify.dao.GenreDAOImpl;
import com.streamify.dao.SongDAOImpl;
import com.streamify.dao.UserDAO;

@Controller
public class AdminController {
    AlbumDAOImpl albumDAO = new AlbumDAOImpl();
    ArtisteDAOImpl artisteDAO = new ArtisteDAOImpl();
    GenreDAOImpl genreDAO = new GenreDAOImpl();
    @RequestMapping(path = "/admin", method = RequestMethod.GET)
    public String gestion_admin(ModelMap model){
        if (SessionService.is_connected_as_admin()) {
            List<Artiste> allArtistes = artisteDAO.getAllArtistes();
            List<Album> allAlbums = albumDAO.getAllAlbums();
            List<Genre> allGenres = genreDAO.getAllGenres();
            List<User> allUsers = UserDAO.getAll();

            model.addAttribute("liste_artistes", allArtistes);
            model.addAttribute("liste_albums", allAlbums);
            model.addAttribute("liste_genres", allGenres);
            model.addAttribute("liste_users", allUsers);

            model.addAttribute("titre", "Administrateur");
            return "gestion_admin";
        } else {
            model.addAttribute("warning_msg", "Accès refusé: Vous devez "+
            " vous connecter en temps qu'Administrateur pour utiliser ses fonctionnalités!");
            return "login";
        }
    }
    @RequestMapping(path = "/ajout_chanson", method = RequestMethod.POST)
    public String ajout_chanson(
        @RequestParam("song_titre") String song_titre,
        @RequestParam("song_artiste") int song_artiste,
        @RequestParam("song_lien") String song_lien,
        @RequestParam("song_album") int song_album,
        @RequestParam("song_genre") int song_genre,
        ModelMap model){
        if (SessionService.is_connected_as_admin()) {
            Artiste artiste = new Artiste();
            artiste.setId(song_artiste);
            Album album = new Album();
            album.setId(song_album);
            album.setArtiste(artiste);
            Song song = new Song();
            Genre genre = new Genre();
            genre.setId(song_genre);

            song.setTitre(song_titre);
            song.setLink(song_lien);
            song.setAlbum(album);
            song.setGenre(genre);
            SongDAOImpl songDAO = new SongDAOImpl();
            boolean reussite = songDAO.insert(song);

            List<Artiste> allArtistes = artisteDAO.getAllArtistes();
            List<Album> allAlbums = albumDAO.getAllAlbums();
            List<Genre> allGenres = genreDAO.getAllGenres();
            List<User> allUsers = UserDAO.getAll();

            model.addAttribute("liste_artistes", allArtistes);
            model.addAttribute("liste_albums", allAlbums);
            model.addAttribute("liste_genres", allGenres);

            model.addAttribute("titre", "Administrateur");
            if (reussite) {
                model.addAttribute("success_msg", "Chanson ajouté");
            } else {
                model.addAttribute("warning_msg", "Échec de l'ajout de la chanson.");
            }
            return "gestion_admin";
        } else {
            model.addAttribute("warning_msg", "Accès refusé: Vous devez "+
            " vous connecter en temps qu'Administrateur pour utiliser ses fonctionnalités!");
            return "login";
        }
    }
    @RequestMapping(path = "/ajout_artiste", method = RequestMethod.POST)
    public String ajout_artiste(
        @RequestParam("artiste_nom") String artiste_nom,
        @RequestParam("artiste_pays") String artiste_pays,
        @RequestParam("artiste_lien") String artiste_lien,
        ModelMap model){
        if (SessionService.is_connected_as_admin()) {
            Artiste artiste = new Artiste();
            artiste.setNom(artiste_nom);
            artiste.setPays(artiste_pays);
            artiste.setImg_link(artiste_lien);
            boolean reussite = artisteDAO.insert(artiste);
            List<Artiste> allArtistes = artisteDAO.getAllArtistes();
            List<Album> allAlbums = albumDAO.getAllAlbums();
            List<Genre> allGenres = genreDAO.getAllGenres();
            List<User> allUsers = UserDAO.getAll();

            model.addAttribute("liste_artistes", allArtistes);
            model.addAttribute("liste_albums", allAlbums);
            model.addAttribute("liste_genres", allGenres);
            model.addAttribute("liste_users", allUsers);       

            model.addAttribute("titre", "Administrateur");
            if (reussite) {
                model.addAttribute("success_msg", "Artiste ajouté");
            } else {
                model.addAttribute("warning_msg", "Échec de l'ajout de l'artiste.");
            }
            return "gestion_admin";
        } else {
            model.addAttribute("warning_msg", "Accès refusé: Vous devez "+
            " vous connecter en temps qu'Administrateur pour utiliser ses fonctionnalités!");
            return "login";
        }
    }
    @RequestMapping(path = "/ajout_album", method = RequestMethod.POST)
    public String ajout_album(
        @RequestParam("album_titre") String album_titre,
        @RequestParam("album_artiste") int album_artiste,
        @RequestParam("album_lien") String album_lien,
        ModelMap model){
        if (SessionService.is_connected_as_admin()) {
            Album album = new Album();
            album.setTitre(album_titre);
            Artiste artiste = new Artiste();
            artiste.setId(album_artiste);
            album.setArtiste(artiste);
            album.setImg_link(album_lien);

            boolean reussite = albumDAO.insert(album);

            List<Artiste> allArtistes = artisteDAO.getAllArtistes();
            List<Album> allAlbums = albumDAO.getAllAlbums();
            List<Genre> allGenres = genreDAO.getAllGenres();
            List<User> allUsers = UserDAO.getAll();

            model.addAttribute("liste_artistes", allArtistes);
            model.addAttribute("liste_albums", allAlbums);
            model.addAttribute("liste_genres", allGenres);

            model.addAttribute("titre", "Administrateur");
            if (reussite) {
                model.addAttribute("success_msg", "Album ajouté");
            } else {
                model.addAttribute("warning_msg", "Échec de l'ajout de l'album.");
            }
            return "gestion_admin";
        } else {
            model.addAttribute("warning_msg", "Accès refusé: Vous devez "+
            " vous connecter en temps qu'Administrateur pour utiliser ses fonctionnalités!");
            return "login";
        }
    }
    @RequestMapping(path = "/ajout_genre", method = RequestMethod.POST)
    public String ajout_genre(
        @RequestParam("genre_nom") String genre_nom,
        @RequestParam("genre_lien") String genre_lien,
        ModelMap model){
        if (SessionService.is_connected_as_admin()) {
            Genre genre = new Genre();
            genre.setNom(genre_nom);
            genre.setImg_link(genre_lien);

            boolean reussite = genreDAO.insert(genre);

            List<Artiste> allArtistes = artisteDAO.getAllArtistes();
            List<Album> allAlbums = albumDAO.getAllAlbums();
            List<Genre> allGenres = genreDAO.getAllGenres();
            List<User> allUsers = UserDAO.getAll();

            model.addAttribute("liste_artistes", allArtistes);
            model.addAttribute("liste_albums", allAlbums);
            model.addAttribute("liste_genres", allGenres);
            model.addAttribute("liste_users", allUsers);        

            model.addAttribute("titre", "Administrateur");
            if (reussite) {
                model.addAttribute("success_msg", "Genre ajouté");
            } else {
                model.addAttribute("warning_msg", "Échec de l'ajout du genre.");
            }
            return "gestion_admin";
        } else {
            model.addAttribute("warning_msg", "Accès refusé: Vous devez "+
            " vous connecter en temps qu'Administrateur pour utiliser ses fonctionnalités!");
            return "login";
        }
    }
    @RequestMapping(path = "/delete_user", method = RequestMethod.GET)
    public String delete_user(
        @RequestParam("user_uid") String user_uid,
        ModelMap model){
        if (SessionService.is_connected_as_admin()) {
            boolean reussite = UserDAO.delete(user_uid);

            List<Artiste> allArtistes = artisteDAO.getAllArtistes();
            List<Album> allAlbums = albumDAO.getAllAlbums();
            List<Genre> allGenres = genreDAO.getAllGenres();
            List<User> allUsers = UserDAO.getAll();

            model.addAttribute("liste_artistes", allArtistes);
            model.addAttribute("liste_albums", allAlbums);
            model.addAttribute("liste_genres", allGenres);
            

            

            model.addAttribute("titre", "Administrateur");
            if (reussite) {
                model.addAttribute("success_msg", "Utilisateur supprimé");
            } else {
                model.addAttribute("warning_msg", "Échec de la suppression de l'utilisateur.");
            }
            return "gestion_admin";
        } else {
            model.addAttribute("warning_msg", "Accès refusé: Vous devez "+
            " vous connecter en temps qu'Administrateur pour utiliser ses fonctionnalités!");
            return "login";
        }
    }
    @RequestMapping(path = "/change_role_user", method = RequestMethod.GET)
    public String change_role_user(
        @RequestParam("user_uid") String user_uid,
        ModelMap model){
        if (SessionService.is_connected_as_admin()) {
            boolean reussite = UserDAO.change_role(user_uid);

            List<Artiste> allArtistes = artisteDAO.getAllArtistes();
            List<Album> allAlbums = albumDAO.getAllAlbums();
            List<Genre> allGenres = genreDAO.getAllGenres();
            List<User> allUsers = UserDAO.getAll();

            model.addAttribute("liste_artistes", allArtistes);
            model.addAttribute("liste_albums", allAlbums);
            model.addAttribute("liste_genres", allGenres);
            model.addAttribute("liste_users", allUsers);   

            model.addAttribute("titre", "Administrateur");
            if (reussite) {
                model.addAttribute("success_msg", "Succèss du changement du rôle de l'utilisateur.");
            } else {
                model.addAttribute("warning_msg", "Échec du changement du rôle de l'utilisateur.");
            }
            return "gestion_admin";
        } else {
            model.addAttribute("warning_msg", "Accès refusé: Vous devez "+
            " vous connecter en temps qu'Administrateur pour utiliser ses fonctionnalités!");
            return "login";
        }
    }
}

