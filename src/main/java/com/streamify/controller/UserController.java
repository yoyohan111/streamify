package com.streamify.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.streamify.model.Album;
import com.streamify.model.Artiste;
import com.streamify.model.Genre;
import com.streamify.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.streamify.action.UserAction;
import com.streamify.dao.AlbumDAOImpl;
import com.streamify.dao.ArtisteDAOImpl;
import com.streamify.dao.GenreDAOImpl;
import com.streamify.dao.PlaylistDAOImpl;
import com.streamify.dao.UserDAO;
import com.streamify.model.Album;
import com.streamify.model.Artiste;
import com.streamify.model.Genre;
import java.util.List;

@Controller
public class UserController {
    PlaylistDAOImpl playlistDAO = new PlaylistDAOImpl();
    @RequestMapping(path = "/inscrireUser", method = RequestMethod.POST)
    public String inscription(ModelMap model, @ModelAttribute("form_inscription") User input_user, BindingResult result){
        if (result.hasErrors()){
            model.addAttribute("danger_msg", "ERREUR: Le BindingResult ne fonctionne pas sur SpringMVC!");
            return "inscription";
        }
        if(UserAction.inscrireUser(input_user)){
            model.addAttribute("success_msg", "Merci pour votre inscription "+input_user.getPrenom() +" "+input_user.getNom()+ ". Veuillez maintenant vous connecter.");
            return "login";
        }else{
            model.addAttribute("warning_msg", "Le courriel '"+ input_user.getEmail() +"' est déja utilisé! Veuillez vous connecter <a href='login'><strong>ICI</strong></a>.");
            return "inscription";
        }                    
    }
    @RequestMapping(path = "/loginUser", method = RequestMethod.POST)
    public String loginUser(		
            
        @RequestParam("email") String email,
        @RequestParam("password") String password,
        HttpSession session,
        ModelMap model) {
        AlbumDAOImpl albumDAO = new AlbumDAOImpl();
        ArtisteDAOImpl artisteDAO = new ArtisteDAOImpl();
        GenreDAOImpl genreDAO = new GenreDAOImpl();
        if (email != null && password != null) {
            User user = UserAction.findByEmail(email);
            if(user != null) {
                if (user.isEncodedPasswordMatch(password)) {
                    session.setAttribute("user", user);
                    model.addAttribute("success_msg", "Bonjour "+user.getPrenom() +" "+user.getNom()+ ", vous êtes connecté!");
                    if (user.getRole() == 1) {
                        List<Artiste> allArtistes = artisteDAO.getAllArtistes();
                        List<Album> allAlbums = albumDAO.getAllAlbums();
                        List<Genre> allGenres = genreDAO.getAllGenres();
                        List<User> allUsers = UserDAO.getAll();
            
                        model.addAttribute("liste_artistes", allArtistes);
                        model.addAttribute("liste_albums", allAlbums);
                        model.addAttribute("liste_genres", allGenres);
                        model.addAttribute("liste_users", allUsers);            
                        return "gestion_admin";
                    } else {
                        model.addAttribute("listePlaylist", playlistDAO.getPlaylistsUser(user.getUid()));
                        return "player";
                    }
                } else {
                    model.addAttribute("warning_msg", "Le mot de passe est invalide pour cet utilisateur. SVP réessayer de nouveau.");
                    model.addAttribute("user_email", user.getEmail());
                    return "login";
                }
            } else {
                model.addAttribute("user_email", email);
                model.addAttribute("warning_msg", "Ce courriel n'appartient à aucun utilisateur existant. SVP vous inscrire <a href='inscription'><strong>ICI</strong></a>.");
                return "login";
            }
        } else {
            model.addAttribute("danger_msg", "ERREUR: Un des champs de saisie est vide.");
            return "login";
        }
    }
    @RequestMapping(path = "/logout", method = RequestMethod.GET)
    public String logout(HttpSession session, ModelMap model){
        session.removeAttribute("user");
        model.addAttribute("info_msg", "Vous êtes déconnecté.");
        return "login";
    }
    @RequestMapping(path = "/change_password", method = RequestMethod.POST)
    public String change_password(
        @RequestParam("old_password") String old_pas,
        @RequestParam("new_password") String new_pas,
        HttpSession session,
        ModelMap model) {
        if (session.getAttribute("user") != null) {
            User user = (User)session.getAttribute("user");
            System.out.println(user.toString());
            if (old_pas != new_pas) {
                if (user.isEncodedPasswordMatch(old_pas)) {
                    if (UserAction.updatePassword(user.getUid(),new_pas)) {
                        model.addAttribute("success_msg", "Le mot de passe a été changé!");
                        return "gestion_user";
                    } else {
                        model.addAttribute("danger_msg", "La fonction 'updatePassword' de la classe 'UserAction' n'a pas fonctionné!");
                        return "gestion_user";
                    }
                } else {
                    model.addAttribute("warning_msg", "Votre mot de passe actuel n'est pas valide!");
                    return "gestion_user";
                }
            } else {
                model.addAttribute("warning_msg", "Vous avez entré le même mot de passe...");
                return "gestion_user";
            }
        } else {
            model.addAttribute("warning_msg", "Vous n'êtes pas connecté!");
            return "login";
        }           
    }
}
