package com.streamify.controller;

import com.streamify.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


/**
 *
 * @author Dominik
 */

@Controller
public class HomeController {
    
    @RequestMapping(path = "/", method = RequestMethod.GET)
    public String home(ModelMap model){
        model.addAttribute("titre", "Accueil");
        return "home";
    }
    
    @RequestMapping(path = "/apropos", method = RequestMethod.GET)
    public String apropos(ModelMap model){
        model.addAttribute("titre", "À propos");
        return "apropos";
    }
    
    @RequestMapping(path = "/emplois", method = RequestMethod.GET)
    public String emplois(ModelMap model){
        model.addAttribute("titre", "Emplois");
        return "emplois";
    }
    
    @RequestMapping(path = "/inscription", method = RequestMethod.GET)
    public String inscription(ModelMap model){
        model.addAttribute("titre", "Inscription");
        model.addAttribute("user", new User());
        return "inscription";
    }
    
    @RequestMapping(path = "/login", method = RequestMethod.GET)
    public String login(ModelMap model){
        model.addAttribute("titre", "Connexion");
        return "login";
    }
    @RequestMapping(path = "/user", method = RequestMethod.GET)
    public String user(ModelMap model){
        return "gestion_user";
    }
}
