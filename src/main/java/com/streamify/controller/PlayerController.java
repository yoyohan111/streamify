package com.streamify.controller;

import com.streamify.dao.AlbumDAOImpl;
import com.streamify.dao.PlaylistDAOImpl;
import com.streamify.util.SessionService;
import com.streamify.dao.SongDAOImpl;
import com.streamify.model.Playlist;
import com.streamify.model.User;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class PlayerController {
    SongDAOImpl songDAO = new SongDAOImpl();
    AlbumDAOImpl albumDAO = new AlbumDAOImpl();
    PlaylistDAOImpl playlistDAO = new PlaylistDAOImpl();
    HttpSession session;
    
    private static String[] vue_selon_conn(String vue, String titre_page) {
        String[] result;
        if(SessionService.is_connected_as_user()) {
            result = new String[]{"titre",titre_page,vue};
        } else {
            result = new String[]{"warning_msg",
                "Accès refusé: Vous devez vous connecter en temps " +
                 "qu'utilisateur pour utiliser le lecteur de musique!","login"};
        }
        return result;
    }

    @RequestMapping(path = "/player", method = RequestMethod.GET)
    public String player(ModelMap model, HttpServletRequest request){
        User user = SessionService.get_connected_user();
        String[] infos_vues = vue_selon_conn("player", "Player");
        model.addAttribute(infos_vues[0], infos_vues[1]);
        model.addAttribute("listePlaylist", playlistDAO.getPlaylistsUser(user.getUid()));
        return infos_vues[2];
    }
    @RequestMapping(path = "/playlist", method = RequestMethod.GET)
    public String playlist(ModelMap model, HttpServletRequest request){
        User user = SessionService.get_connected_user();
        String[] infos_vues = vue_selon_conn("playlist", "Playlist");
        model.addAttribute(infos_vues[0], infos_vues[1]);
        model.addAttribute("listePlaylist", playlistDAO.getPlaylistsUser(user.getUid()));
        return infos_vues[2];
    }
    @RequestMapping(path = "/album", method = RequestMethod.GET)
    public String album(ModelMap model, HttpServletRequest request){
        User user = SessionService.get_connected_user();
        String[] infos_vues = vue_selon_conn("album", "Album");
        model.addAttribute(infos_vues[0], infos_vues[1]);
        model.addAttribute("listePlaylist", playlistDAO.getPlaylistsUser(user.getUid()));
        return infos_vues[2];
    }

    @RequestMapping(path = "/decouvrir", method = RequestMethod.GET)
    public String decouvrir(ModelMap model, HttpServletRequest request){
        User user = SessionService.get_connected_user();
        String[] infos_vues = vue_selon_conn("decouvrir", "Découvrir");
        model.addAttribute(infos_vues[0], infos_vues[1]);
        model.addAttribute("listePlaylist", playlistDAO.getPlaylistsUser(user.getUid()));
        return infos_vues[2];
    }
    @RequestMapping(path = "/recherche", method = RequestMethod.GET)
    public ModelAndView recherche(@RequestParam("categorie") Optional<String> categorie,@RequestParam("recherche_text") Optional<String> recherche, HttpServletRequest request){
        User user = SessionService.get_connected_user();
        boolean search = request.getParameterMap().containsKey("categorie");
        if(search){
            switch(categorie.get()){
                case "electro" -> {
                    
                    ModelAndView model = new ModelAndView("recherche");
                    model.addObject("titre", "Recherche");
                    model.addObject("listSongsByGenre", songDAO.getSongsByGenre(1));
                    model.addObject("listAlbumsByGenre",albumDAO.getAlbumByGenre(1));
                    model.addObject("listePlaylist", playlistDAO.getPlaylistsUser(user.getUid()));
                    model.addObject("jsonList", songDAO.getListeSongJSON(songDAO.getSongsByGenre(1)));
                    return model;
                }
                case "hiphop" -> {
                    ModelAndView model = new ModelAndView("recherche");
                    model.addObject("titre", "Recherche");
                    model.addObject("listSongsByGenre", songDAO.getSongsByGenre(2));
                    model.addObject("listAlbumsByGenre",albumDAO.getAlbumByGenre(2));
                    model.addObject("listePlaylist", playlistDAO.getPlaylistsUser(user.getUid()));
                    model.addObject("jsonList", songDAO.getListeSongJSON(songDAO.getSongsByGenre(2)));
                    return model;
                }
                case "jazz" -> {
                    ModelAndView model = new ModelAndView("recherche");
                    model.addObject("titre", "Recherche");
                    model.addObject("listSongsByGenre", songDAO.getSongsByGenre(3));
                    model.addObject("listAlbumsByGenre",albumDAO.getAlbumByGenre(3));
                    model.addObject("listePlaylist", playlistDAO.getPlaylistsUser(user.getUid()));
                    model.addObject("jsonList", songDAO.getListeSongJSON(songDAO.getSongsByGenre(3)));
                    return model;
                }
                case "rock" -> {
                    ModelAndView model = new ModelAndView("recherche");
                    model.addObject("titre", "Recherche");
                    model.addObject("listSongsByGenre", songDAO.getSongsByGenre(4));
                    model.addObject("listAlbumsByGenre",albumDAO.getAlbumByGenre(4));
                    model.addObject("listePlaylist", playlistDAO.getPlaylistsUser(user.getUid()));
                    model.addObject("jsonList", songDAO.getListeSongJSON(songDAO.getSongsByGenre(4)));
                    return model;
                }

            }
            
        }
        search = request.getParameterMap().containsKey("recherche_text");
        if(search){
            ModelAndView model = new ModelAndView("recherche");
            model.addObject("titre", "Recherche");
            model.addObject("listSongsByGenre", songDAO.getSongsByGenre(4));
            model.addObject("listAlbumsByGenre",albumDAO.getAlbumByGenre(4));
            model.addObject("listePlaylist", playlistDAO.getPlaylistsByName(recherche.get()));
            model.addObject("jsonList", songDAO.getListeSongJSON(songDAO.getSongsByGenre(4)));
        }
        ModelAndView model = new ModelAndView("recherche");
        model.addObject("titre", "Recherche");
        model.addObject("listePlaylist", playlistDAO.getPlaylistsUser(user.getUid()));
        return model;
    }
    @RequestMapping(path = "/librairie", method = RequestMethod.GET)
    public String librairie(ModelMap model, HttpServletRequest request){
        User user = SessionService.get_connected_user();
        String[] infos_vues = vue_selon_conn("librairie", "Librairie");
        model.addAttribute(infos_vues[0], infos_vues[1]);
        model.addAttribute("listePlaylist", playlistDAO.getPlaylistsUser(user.getUid()));
        model.addAttribute("listeAlbums", albumDAO.getAllAlbums());
        model.addAttribute("listeSongs", songDAO.getAllSongs());
        model.addAttribute("jsonList", songDAO.getListeSongJSON(songDAO.getAllSongs()));
        return infos_vues[2];
    }
    @RequestMapping(path = "/favoris", method = RequestMethod.GET)
    public String favoris(ModelMap model, HttpServletRequest request){
        User user = SessionService.get_connected_user();
        String[] infos_vues = vue_selon_conn("favoris", "Favoris");
        model.addAttribute(infos_vues[0], infos_vues[1]);
        model.addAttribute("listePlaylist", playlistDAO.getPlaylistsUser(user.getUid()));
        return infos_vues[2];
    }

    @RequestMapping(path = "/new_playlist", method = RequestMethod.GET)
    public String new_playlist(
        @RequestParam("titre_playlist") String titre_playlist,
        @RequestParam("id_playlist") int id_playlist,
        ModelMap model){
        if (SessionService.is_connected_as_user()) {
            User user = SessionService.get_connected_user();
            Playlist playlist = new Playlist();
            playlist.setId_user(user.getUid());
            playlist.setTitre(titre_playlist);
            
            boolean reussite = playlistDAO.insert(playlist);

            model.addAttribute("playlist", playlistDAO.getById(playlist.getId()));
            model.addAttribute("listePlaylist", playlistDAO.getPlaylistsUser(user.getUid()));  
            model.addAttribute("jsonList", songDAO.getListeSongJSON(playlistDAO.getSongsByPlaylistId(id_playlist)));
            model.addAttribute("titre", "Playlist");
            if (reussite) {
                model.addAttribute("success_msg", "Playlist '"+playlist.getTitre()+" Ajouté!");
            } else {
                model.addAttribute("warning_msg", "Échec de l'ajout de la playlist.");
            }
            return "playlist?id_playlist=id_playlist";
        } else {
            model.addAttribute("warning_msg", "Accès refusé: Vous devez "+
            " vous connecter en temps qu'Administrateur pour utiliser ses fonctionnalités!");
            return "login";
        }
    }
    @RequestMapping(path = "/ajout_song_to_playlist", method = RequestMethod.GET)
    public String ajout_song_playlist(
        @RequestParam("id_song") int id_song,
        @RequestParam("id_playlist") int id_playlist,
        ModelMap model){
        if (SessionService.is_connected_as_user()) {
            User user = SessionService.get_connected_user();
            Playlist playlist = playlistDAO.getById(id_playlist);

            boolean reussite = playlistDAO.insertSongInPlaylist(id_song, playlist.getId());

            model.addAttribute("playlist", playlistDAO.getById(id_playlist));
            model.addAttribute("listePlaylist", playlistDAO.getPlaylistsUser(user.getUid()));  
            model.addAttribute("jsonList", songDAO.getListeSongJSON(playlistDAO.getSongsByPlaylistId(id_playlist)));
            model.addAttribute("titre", "Playlist");
            if (reussite) {
                model.addAttribute("success_msg", "Playlist '"+playlist.getTitre()+" Ajouté!");
            } else {
                model.addAttribute("warning_msg", "Échec de l'ajout de la playlist.");
            }
            return "playlist?id_playlist=id_playlist";
        } else {
            model.addAttribute("warning_msg", "Accès refusé: Vous devez "+
            " vous connecter en temps qu'Administrateur pour utiliser ses fonctionnalités!");
            return "login";
        }
    }
}
