/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.streamify.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import java.util.Random;
import com.streamify.dao.SongDAOImpl;
import com.streamify.dao.ArtisteDAO;
import com.streamify.dao.ArtisteDAOImpl;


/**
 *
 * @author Dominik
 */
@Controller
public class CheckController {
    
    
    SongDAOImpl songDAO = new SongDAOImpl();
    ArtisteDAOImpl artisteDAO = new ArtisteDAOImpl();
    
    @RequestMapping(path = "/check", method = RequestMethod.GET)
    public ModelAndView check(){
        ModelAndView model = new ModelAndView("check");
        model.addObject("artiste", artisteDAO.getById(1));
        model.addObject("link", songDAO.getSongLink(1));
        model.addObject("song", songDAO.getSongById(2));
        model.addObject("genre", songDAO.getSongGenre(1));
        model.addObject("listSongs", songDAO.getAllSongs());
        model.addObject("jsonList", songDAO.getListeSongJSON(songDAO.getAllSongs()));
        model.addObject("msg","Bienvenue au test du lecteur!");
        return model;
    }
}
