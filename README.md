# streamify

### Projet d'école: créer une clone de _Spotify_ en Java EE

## Configuration et Installation de l'environnement

**Ce projet Maven importe ses dépendances automatiquement selon le fichier [pom.xml](pom.xml). Cela veux dire que si vous n'avez pas les versions exactes indiqués dans ce fichier, l'exécution ne fonctionnera pas!**
- framework `Spring mvc` utilisé **_(v5.3.0)_** 
### Programmes obligatoires

- **Maven (v3.6.3):**
  - Le télécharger d'ici : <https://maven.apache.org/download.cgi>
  - Extraire le zip dans votre C: par example
  - le mettre dans les variables d'environnements de Windows
- **Tomcat server (v9 utilisé):**
  - Le télécharger d'ici _(prendre le `Core`)_: <https://tomcat.apache.org/download-90.cgi>
  - le mettre dans les variables d'environnements aussi

### La base de donnée

- Tout les instructions sont dans le fichier [INSTRUCTIONS_BD](BD/INSTRUCTIONS_BD.md) du dossier `BD` du projet, qui contiens aussi toutes les tables.

### Sur l'IDE _NetBeans 12_

 **(vous pouvez toujours développer avec VSCode, mais son exécution du projet ne fonctionne pas fiablement...)**

1. Dans l'onglet `Services` en haut à gauche de l'IDE, ajouter le serveur Tomcat.
    - Il faut avoir navigué dans le dossier d'installation (normalement `C:\Program Files\Apache Software Foundation\Tomcat 9.0`) et juste ouvrir de dossier comme administrateur pour que NetBeans puisse le lire correctement.
    - Ajouter le serveur dans NetBeans en lui spécifiant le dossier d'installation de Tomcat.
3. Cliquer droit sur le projet et faire `run`

## Par

- [Dominik Dupuis](https://github.com/DominikDupuis)
- [Yohan Gagnon-Knackstedt](https://github.com/y11ohan)
